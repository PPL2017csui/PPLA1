cd "$(dirname "$0")"

if [[ -z "$1" ]]; then
	name=kape
else
	name=kape_$1
fi

if [[ -z "$2" ]]; then
        port=8000
else
        port=$2
fi

if [[ -z "$3" ]]; then
        ssh=8022
else
        ssh=$3
fi

echo Running Docker container with name $name

if [[ $(docker ps -a -f "name=$name" --format '{{.Names}}') == $name ]]; then
	docker start $name
	docker exec $name bash /home/kape/provision/start-service.sh
else
	docker run --name $name -p $port:8000 -p $ssh:8022 -v $(realpath ../):/home/kape zmajdy/kape  bash /home/kape/provision/start-service.sh
fi
