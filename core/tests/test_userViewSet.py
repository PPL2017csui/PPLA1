from django.contrib.auth.models import User
from rest_framework import status
from rest_framework.test import APITestCase


class TestUserViewSet(APITestCase):
    def test_me_success(self):
        superuser = User.objects.create_superuser('dummy.user', 'dummy.user@user.com', 'lalala123')
        self.client.force_authenticate(user=superuser)

        url = '/api/users/me/'
        response = self.client.get(url, format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_users_fail(self):
        url = '/api/users/'
        response = self.client.get(url, format='json')
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)

        response = self.client.put(url, format='json')
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)

        response = self.client.patch(url, format='json')
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)

    def test_user_students_success(self):
        superuser = User.objects.create_superuser('dummy.user', 'dummy.user@user.com', 'lalala123')
        self.client.force_authenticate(user=superuser)

        url = '/api/students/'
        response = self.client.get(url, format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_user_students_fail(self):
        url = '/api/students/'
        response = self.client.get(url, format='json')
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)

        response = self.client.post(url, format='json')
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)

        response = self.client.put(url, format='json')
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)

        response = self.client.patch(url, format='json')
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)
