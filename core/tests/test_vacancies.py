from datetime import datetime

import requests_mock
from django.contrib.auth.models import User
from rest_framework import status
from rest_framework.test import APITestCase

from core.models.accounts import Company, Student, Supervisor
from core.models.vacancies import Vacancy, Application


class ApplicationTests(APITestCase):
    @requests_mock.Mocker()
    def test_application_list(self, m):
        m.post('https://api.cs.ui.ac.id/authentication/ldap/v2/', json={
            "username": "dummy.mahasiswa",
            "nama": "Dummy Mahasiswa",
            "state": 1,
            "kode_org": "01.00.12.01:mahasiswa",
            "kodeidentitas": "1234567890",
            "nama_role": "mahasiswa"
        }, status_code=200)
        m.get('https://api.cs.ui.ac.id/siakngcs/mahasiswa/1234567890/', json={
            "kota_lahir": "kota_kota",
            "tgl_lahir": "2017-12-31",
            "program": [{
                "nm_org": "Ilmu Informasi",
                "angkatan": "2017"
            }]
        }, status_code=200)

        url = '/api/login/'

        response = self.client.post(url, {'username': 'dummy.mahasiswa', 'password': 'lalala', 'login-type': 'sso-ui'},
                                    format='json')
        student_id = response.data.get('student').get('id')

        url = '/api/students/' + str(student_id) + '/applied-vacancies/'
        response = self.client.get(url)

        self.assertEqual(response.status_code, status.HTTP_200_OK)

    @requests_mock.Mocker()
    def test_application_create_and_delete(self, m):
        m.post('https://api.cs.ui.ac.id/authentication/ldap/v2/', json={
            "username": "dummy.mahasiswa",
            "nama": "Dummy Mahasiswa",
            "state": 1,
            "kode_org": "01.00.12.01:mahasiswa",
            "kodeidentitas": "1234567890",
            "nama_role": "mahasiswa"
        }, status_code=200)
        m.get('https://api.cs.ui.ac.id/siakngcs/mahasiswa/1234567890/', json={
            "kota_lahir": "kota_kota",
            "tgl_lahir": "2017-12-31",
            "program": [{
                "nm_org": "Ilmu Informasi",
                "angkatan": "2017"
            }]
        }, status_code=200)

        url = '/api/login/'

        response = self.client.post(url, {'username': 'dummy.mahasiswa', 'password': 'lalala', 'login-type': 'sso-ui'},
                                    format='json')
        student_id = response.data.get('student').get('id')

        new_user = User.objects.create_user('dummy.company', 'dummy.company@company.com', 'lalala123')
        new_company = Company.objects.create(user=new_user, description="lalala", status=Company.VERIFIED, logo=None,
                                             address=None)
        new_vacancy = Vacancy.objects.create(company=new_company, verified=True, open_time=datetime.fromtimestamp(0),
                                             description="lalala", close_time=datetime.today())

        url = '/api/students/' + str(student_id) + '/applied-vacancies/'
        response = self.client.post(url, {'vacancy_id': new_vacancy.pk, 'cover_letter': 'this is a cover letter.'},
                                    format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)

        url = '/api/students/' + str(student_id) + '/applied-vacancies/' + str(new_vacancy.pk) + '/'
        response = self.client.delete(url)
        self.assertEqual(response.status_code, status.HTTP_200_OK)


class BookmarkApplicationTests(APITestCase):
    @requests_mock.Mocker()
    def test_application_list(self, m):
        m.post('https://api.cs.ui.ac.id/authentication/ldap/v2/', json={
            "username": "dummy.mahasiswa",
            "nama": "Dummy Mahasiswa",
            "state": 1,
            "kode_org": "01.00.12.01:mahasiswa",
            "kodeidentitas": "1234567890",
            "nama_role": "mahasiswa"
        }, status_code=200)
        m.get('https://api.cs.ui.ac.id/siakngcs/mahasiswa/1234567890/', json={
            "kota_lahir": "kota_kota",
            "tgl_lahir": "2017-12-31",
            "program": [{
                "nm_org": "Ilmu Informasi",
                "angkatan": "2017"
            }]
        }, status_code=200)

        url = '/api/login/'

        response = self.client.post(url, {'username': 'dummy.mahasiswa', 'password': 'lalala', 'login-type': 'sso-ui'},
                                    format='json')
        student_id = response.data.get('student').get('id')

        url = '/api/students/' + str(student_id) + '/bookmarked-vacancies/'
        response = self.client.get(url)

        self.assertEqual(response.status_code, status.HTTP_200_OK)

    @requests_mock.Mocker()
    def test_application_create_and_delete(self, m):
        m.post('https://api.cs.ui.ac.id/authentication/ldap/v2/', json={
            "username": "dummy.mahasiswa",
            "nama": "Dummy Mahasiswa",
            "state": 1,
            "kode_org": "01.00.12.01:mahasiswa",
            "kodeidentitas": "1234567890",
            "nama_role": "mahasiswa"
        }, status_code=200)
        m.get('https://api.cs.ui.ac.id/siakngcs/mahasiswa/1234567890/', json={
            "kota_lahir": "kota_kota",
            "tgl_lahir": "2017-12-31",
            "program": [{
                "nm_org": "Ilmu Informasi",
                "angkatan": "2017"
            }]
        }, status_code=200)

        url = '/api/login/'

        response = self.client.post(url, {'username': 'dummy.mahasiswa', 'password': 'lalala', 'login-type': 'sso-ui'},
                                    format='json')
        student_id = response.data.get('student').get('id')

        new_user = User.objects.create_user('dummy.company2', 'dummy.compan2y@company.com', 'lalala123')
        new_company = Company.objects.create(user=new_user, description="lalala", status=Company.VERIFIED, logo=None,
                                             address=None)
        new_vacancy = Vacancy.objects.create(company=new_company, verified=True, open_time=datetime.fromtimestamp(0),
                                             description="lalala", close_time=datetime.today())

        url = '/api/students/' + str(student_id) + '/bookmarked-vacancies/'
        response = self.client.post(url, {'vacancy_id': new_vacancy.pk}, format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)

        url = '/api/students/' + str(student_id) + '/bookmarked-vacancies/' + str(new_vacancy.pk) + '/'
        response = self.client.delete(url)
        self.assertEqual(response.status_code, status.HTTP_200_OK)


class VacancyTest(APITestCase):
    def test_verified_vacancy_list(self):
        superuser = User.objects.create_superuser('dummy.company', 'dummy.company@company.com', 'lalala123')
        self.client.force_authenticate(user=superuser)

        url = '/api/vacancies/'
        response = self.client.get(url, format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_unverified_vacancy_list(self):
        superuser = User.objects.create_superuser('dummy.company', 'dummy.company@company.com', 'lalala123')
        self.client.force_authenticate(user=superuser)

        url = '/api/vacancies/?verified=false'
        response = self.client.get(url, format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_fail_on_unverified_user_vacancy_list(self):
        url = '/api/vacancies/'
        response = self.client.post(url, format='json')
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)


class CompanyListsTests(APITestCase):
    def test_company_vacancy_list(self):
        new_user = User.objects.create_user('dummy.company3', 'dummy.company3@company.com', 'lalala123')
        new_company = Company.objects.create(user=new_user, description="lalala", status=Company.VERIFIED, logo=None,
                                             address=None)

        self.client.force_authenticate(new_user)

        url = '/api/companies/' + str(new_company.pk) + '/vacancies/'
        response = self.client.get(url, format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_company_vacancy_list_unauthorized(self):
        new_user = User.objects.create_user('dummy.company3', 'dummy.company3@company.com', 'lalala123')
        new_company = Company.objects.create(user=new_user, description="lalala", status=Company.VERIFIED, logo=None,
                                             address=None)

        new_user2 = User.objects.create_user('dummy.companyzxc', 'dummy.companyzxc@company.com', 'lalala123')
        new_company2 = Company.objects.create(user=new_user2, description="lalalaasdsad", status=Company.VERIFIED,
                                              logo=None,
                                              address=None)

        self.client.force_authenticate(new_user2)

        url = '/api/companies/' + str(new_company.pk) + '/vacancies/'
        response = self.client.get(url, format='json')
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)

    def test_company_application_list(self):
        new_user = User.objects.create_user('dummy.company4', 'dummy.company4@company.com', 'lalala123')
        new_company = Company.objects.create(user=new_user, description="lalala", status=Company.VERIFIED, logo=None,
                                             address=None)

        self.client.force_authenticate(new_user)

        url = '/api/companies/' + str(new_company.pk) + '/applications/'
        response = self.client.get(url, format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_company_application_list_with_status(self):
        new_user = User.objects.create_user('dummy.company4', 'dummy.company4@company.com', 'lalala123')
        new_company = Company.objects.create(user=new_user, description="lalala", status=Company.VERIFIED, logo=None,
                                             address=None)

        self.client.force_authenticate(new_user)

        url = '/api/companies/' + str(new_company.pk) + '/applications/?status=0'
        response = self.client.get(url, format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_company_application_list_unauthorized(self):
        new_user = User.objects.create_user('dummy.company3', 'dummy.company3@company.com', 'lalala123')
        new_company = Company.objects.create(user=new_user, description="lalala", status=Company.VERIFIED, logo=None,
                                             address=None)

        new_user2 = User.objects.create_user('dummy.companyzxc', 'dummy.companyzxc@company.com', 'lalala123')
        new_company2 = Company.objects.create(user=new_user2, description="lalalaasdsad", status=Company.VERIFIED,
                                              logo=None,
                                              address=None)

        self.client.force_authenticate(new_user2)

        url = '/api/companies/' + str(new_company.pk) + '/applications/'
        response = self.client.get(url, format='json')
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)

    def test_company_application_list_by_vacancy(self):
        new_user = User.objects.create_user('dummy.company3', 'dummy.company3@company.com', 'lalala123')
        new_company = Company.objects.create(user=new_user, description="lalala", status=Company.VERIFIED, logo=None,
                                             address=None)

        new_user2 = User.objects.create_user('dummy.company4', 'dummy.company4@company.com', 'lalala123')
        new_student = Student.objects.create(user=new_user2, npm=1234123412)

        new_vacancy = Vacancy.objects.create(company=new_company, verified=True, open_time=datetime.fromtimestamp(0),
                                             description="lalala", close_time=datetime.today())
        new_app = Application.objects.create(student=new_student, vacancy=new_vacancy, cover_letter="asdasdasd")

        self.client.force_authenticate(new_user)

        url = '/api/companies/' + str(new_company.pk) + '/applications/' + str(new_vacancy.pk) + '/by_vacancy/'
        response = self.client.get(url, format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_company_application_list_by_vacancy_unauthorized_1(self):
        new_user = User.objects.create_user('dummy.company3', 'dummy.company3@company.com', 'lalala123')
        new_company = Company.objects.create(user=new_user, description="lalala", status=Company.VERIFIED, logo=None,
                                             address=None)

        new_user2 = User.objects.create_user('dummy.student', 'dummy.company3@company.com', 'lalala123')
        new_student = Student.objects.create(user=new_user2, npm=1234123412)

        new_vacancy = Vacancy.objects.create(company=new_company, verified=True, open_time=datetime.fromtimestamp(0),
                                             description="lalala", close_time=datetime.today())
        new_app = Application.objects.create(student=new_student, vacancy=new_vacancy, cover_letter="asdasdasd")

        new_user3 = User.objects.create_user('dummy.company4', 'dummy.company4@company.com', 'lalala123')
        new_company3 = Company.objects.create(user=new_user3, description="lalala", status=Company.VERIFIED, logo=None,
                                              address=None)

        self.client.force_authenticate(new_user3)

        url = '/api/companies/' + str(new_company.pk) + '/applications/' + str(new_vacancy.pk) + '/by_vacancy/'
        response = self.client.get(url, format='json')
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)

    def test_company_application_list_by_vacancy_unauthorized_2(self):
        new_user = User.objects.create_user('dummy.company3', 'dummy.company3@company.com', 'lalala123')
        new_company = Company.objects.create(user=new_user, description="lalala", status=Company.VERIFIED, logo=None,
                                             address=None)

        new_user2 = User.objects.create_user('dummy.student', 'dummy.company3@company.com', 'lalala123')
        new_student = Student.objects.create(user=new_user2, npm=1234123412)

        new_vacancy = Vacancy.objects.create(company=new_company, verified=True, open_time=datetime.fromtimestamp(0),
                                             description="lalala", close_time=datetime.today())
        new_app = Application.objects.create(student=new_student, vacancy=new_vacancy, cover_letter="asdasdasd")

        new_user3 = User.objects.create_user('dummy.company4', 'dummy.company4@company.com', 'lalala123')
        new_company3 = Company.objects.create(user=new_user3, description="lalala", status=Company.VERIFIED, logo=None,
                                              address=None)

        self.client.force_authenticate(new_user3)

        url = '/api/companies/' + str(new_company3.pk) + '/applications/' + str(new_vacancy.pk) + '/by_vacancy/'
        response = self.client.get(url, format='json')
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)

    def test_company_application_list_by_vacancy_with_status(self):
        new_user = User.objects.create_user('dummy.company3', 'dummy.company3@company.com', 'lalala123')
        new_company = Company.objects.create(user=new_user, description="lalala", status=Company.VERIFIED, logo=None,
                                             address=None)

        new_user2 = User.objects.create_user('dummy.company4', 'dummy.company4@company.com', 'lalala123')
        new_student = Student.objects.create(user=new_user2, npm=1234123412)

        new_vacancy = Vacancy.objects.create(company=new_company, verified=True, open_time=datetime.fromtimestamp(0),
                                             description="lalala", close_time=datetime.today())
        new_app = Application.objects.create(student=new_student, vacancy=new_vacancy, cover_letter="asdasdasd")

        self.client.force_authenticate(new_user)

        url = '/api/companies/' + str(new_company.pk) + '/applications/' + str(new_vacancy.pk) + '/by_vacancy/?status=2'
        response = self.client.get(url, format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_company_application_list_by_vacancy_with_bad_status_string(self):
        new_user = User.objects.create_user('dummy.company3', 'dummy.company3@company.com', 'lalala123')
        new_company = Company.objects.create(user=new_user, description="lalala", status=Company.VERIFIED, logo=None,
                                             address=None)

        new_user2 = User.objects.create_user('dummy.company4', 'dummy.company4@company.com', 'lalala123')
        new_student = Student.objects.create(user=new_user2, npm=1234123412)

        new_vacancy = Vacancy.objects.create(company=new_company, verified=True, open_time=datetime.fromtimestamp(0),
                                             description="lalala", close_time=datetime.today())
        new_app = Application.objects.create(student=new_student, vacancy=new_vacancy, cover_letter="asdasdasd")

        self.client.force_authenticate(new_user)

        url = '/api/companies/' + str(new_company.pk) + '/applications/' + str(
            new_vacancy.pk) + '/by_vacancy/?status=lalala'
        response = self.client.get(url, format='json')
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

    def test_company_application_list_by_vacancy_with_bad_status_negative(self):
        new_user = User.objects.create_user('dummy.company3', 'dummy.company3@company.com', 'lalala123')
        new_company = Company.objects.create(user=new_user, description="lalala", status=Company.VERIFIED, logo=None,
                                             address=None)

        new_user2 = User.objects.create_user('dummy.company4', 'dummy.company4@company.com', 'lalala123')
        new_student = Student.objects.create(user=new_user2, npm=1234123412)

        new_vacancy = Vacancy.objects.create(company=new_company, verified=True, open_time=datetime.fromtimestamp(0),
                                             description="lalala", close_time=datetime.today())
        new_app = Application.objects.create(student=new_student, vacancy=new_vacancy, cover_letter="asdasdasd")

        self.client.force_authenticate(new_user)

        url = '/api/companies/' + str(new_company.pk) + '/applications/' + str(
            new_vacancy.pk) + '/by_vacancy/?status=-1'
        response = self.client.get(url, format='json')
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

    def test_company_application_list_by_vacancy_with_bad_status_large(self):
        new_user = User.objects.create_user('dummy.company3', 'dummy.company3@company.com', 'lalala123')
        new_company = Company.objects.create(user=new_user, description="lalala", status=Company.VERIFIED, logo=None,
                                             address=None)

        new_user2 = User.objects.create_user('dummy.company4', 'dummy.company4@company.com', 'lalala123')
        new_student = Student.objects.create(user=new_user2, npm=1234123412)

        new_vacancy = Vacancy.objects.create(company=new_company, verified=True, open_time=datetime.fromtimestamp(0),
                                             description="lalala", close_time=datetime.today())
        new_app = Application.objects.create(student=new_student, vacancy=new_vacancy, cover_letter="asdasdasd")

        self.client.force_authenticate(new_user)

        url = '/api/companies/' + str(new_company.pk) + '/applications/' + str(new_vacancy.pk) + '/by_vacancy/?status=5'
        response = self.client.get(url, format='json')
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)


class SupervisorStudentApplicationTests(APITestCase):
    def test_list_student_application(self):
        new_user = User.objects.create_user('dummy.supervisor', 'dummy.supervisor@asd.asd', 'lalala123')
        new_supervisor = Supervisor.objects.create(user=new_user, nip=1212121212)
        self.client.force_authenticate(user=new_user)

        url = '/api/applications/'
        response = self.client.get(url, format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_list_student_application_unauthorized(self):
        new_user = User.objects.create_user('dummy.supervisor', 'dummy.supervisor@asd.asd', 'lalala123')
        self.client.force_authenticate(user=new_user)

        url = '/api/applications/'
        response = self.client.get(url, format='json')
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)


class SupervisorApprovalTests(APITestCase):
    def test_supervisor_approve_vacancy(self):
        new_user = User.objects.create_user('dummy.supervisor', 'dummy.supervisor@asd.asd', 'lalala123')
        new_supervisor = Supervisor.objects.create(user=new_user, nip=1212121212)
        self.client.force_authenticate(user=new_user)

        new_user2 = User.objects.create_user('dummy.company2', 'dummy.compan2y@company.com', 'lalala123')
        new_company2 = Company.objects.create(user=new_user2, description="lalala", status=Company.VERIFIED, logo=None,
                                              address=None)
        new_vacancy2 = Vacancy.objects.create(company=new_company2, verified=False, open_time=datetime.fromtimestamp(0),
                                              description="lalala", close_time=datetime.today())

        url = '/api/vacancies/' + str(new_vacancy2.pk) + '/verify/'
        response = self.client.patch(url, {'verified': True}, format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)

        retrieve_vacancy = Vacancy.objects.get(pk=new_vacancy2.pk)
        self.assertEqual(retrieve_vacancy.verified, True)

    def test_unauthorized_approve_vacancy(self):
        new_user = User.objects.create_user('dummy.companyz', 'dummy.companyz@company.com', 'lalala123')
        new_company = Company.objects.create(user=new_user, description="lalalaz", status=Company.VERIFIED, logo=None,
                                             address=None)
        self.client.force_authenticate(user=new_user)

        new_vacancy = Vacancy.objects.create(company=new_company, verified=False, open_time=datetime.fromtimestamp(0),
                                             description="lalala", close_time=datetime.today())

        url = '/api/vacancies/' + str(new_vacancy.pk) + '/verify/'
        response = self.client.patch(url, format='json')
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)
        self.assertEqual(new_vacancy.verified, False)
