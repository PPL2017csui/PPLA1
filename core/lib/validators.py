import os
from django.core.exceptions import ValidationError
from kape.settings import MAX_UPLOAD_SIZE


def validate_file(value, valid_extensions):
    ext = os.path.splitext(value.name)[1]  # [0] returns path+filename
    if not ext.lower() in valid_extensions:
        raise ValidationError(u'Unsupported file extension.')

    if value._size > MAX_UPLOAD_SIZE:
        raise ValidationError(u'File too large.')


def validate_document_file_extension(value):
    validate_file(value, ['.pdf', '.doc', '.docx', '.jpg', '.png', '.xlsx', '.xls'])


def validate_image_file_extension(value):
    validate_file(value, ['.jpeg', '.jpg', '.png', '.JPG', '.JPEG'])
