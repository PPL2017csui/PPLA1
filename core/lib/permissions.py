from rest_framework import permissions
from rest_framework.exceptions import PermissionDenied

from core.models import Company
from core.models import Student
from core.models import Supervisor
from core.models import Application
from core.models import Vacancy


def is_admin_or_student(user):
    return user.is_superuser or user.is_staff or hasattr(user, "student")


def is_admin_or_company(user):
    if user.is_superuser or user.is_staff:
        return True

    if not hasattr(user, "company") or user.company.status != Company.VERIFIED:
        raise PermissionDenied("This account is not valid company account or has not been verified")

    return True


def is_admin_or_supervisor(user):
    return user.is_superuser or user.is_staff or hasattr(user, "supervisor")


def is_admin_or_supervisor_or_company(user):
    return user.is_superuser or user.is_staff or hasattr(user, "supervisor") or hasattr(user, "company")


class IsAdminOrSelfOrReadOnly(permissions.BasePermission):
    def has_object_permission(self, request, view, obj):
        if request.method in permissions.SAFE_METHODS:
            return True
        if request.user.is_superuser or request.user.is_staff:
            return True
        # Instance must have an attribute named `user` or be `user`
        if hasattr(obj, "user"):
            return obj.user == request.user
        return obj == request.user


class IsAdminOrStudent(permissions.BasePermission):
    def has_permission(self, request, view):
        return is_admin_or_student(request.user)

    def has_object_permission(self, request, view, obj):
        user = request.user
        if user.is_superuser or user.is_staff:
            return True
        student = None
        if isinstance(obj, Student):
            student = obj
        elif hasattr(obj, "student"):
            student = obj.student
        else:
            raise PermissionDenied(
                "Checking student permission on object {} not associated with Student"
                    .format(type(obj.__name__))
            )

        return hasattr(user, "student") and user.student == student


class IsAdminOrSupervisor(permissions.BasePermission):
    def has_permission(self, request, view):
        return is_admin_or_supervisor(request.user)

    def has_object_permission(self, request, view, obj):
        user = request.user
        if user.is_superuser or user.is_staff:
            return True
        supervisor = None
        if isinstance(obj, Supervisor):
            supervisor = obj
        elif hasattr(obj, "supervisor"):
            supervisor = obj.supervisor
        else:
            raise PermissionDenied(
                "Checking supervisor permission on object {} not associated with Supervisor"
                    .format(type(obj.__name__))
            )

        return hasattr(user, "supervisor") and user.supervisor == supervisor


class IsAdminOrCompany(permissions.BasePermission):
    def has_permission(self, request, view):
        return is_admin_or_company(request.user)

    def has_object_permission(self, request, view, obj):
        user = request.user
        if user.is_superuser or user.is_staff:
            return True
        company = None
        if isinstance(obj, Company):
            company = obj
        elif hasattr(obj, "company"):
            company = obj.company
        else:
            raise PermissionDenied(
                "Checking company permission on object {} not associated with Company"
                    .format(type(obj.__name__))
            )

        return hasattr(user, "company") and user.company == company


class IsAdminOrSupervisorOrCompany(permissions.BasePermission):
    def has_permission(self, request, view):
        return is_admin_or_supervisor_or_company(request.user)


class IsAdminOrSupervisorOrCompanyOrSelf(permissions.IsAuthenticated):
    def has_object_permission(self, request, view, obj):
        user = request.user
        if user.is_superuser or user.is_staff or hasattr(user, "company") or hasattr(user, "supervisor"):
            return True
        if hasattr(user, "student"):
            if isinstance(obj, Student):
                student = obj
            elif hasattr(obj, "student"):
                student = obj.student
            else:
                raise PermissionDenied(
                    "Checking student permission on object {} not associated with Student"
                        .format(type(obj.__name__))
                )
            return hasattr(user, "student") and user.student == student
        return False


class IsAdminOrVacancyOwner(permissions.BasePermission):
    def has_permission(self, request, view):
        return is_admin_or_company(request.user)

    def has_object_permission(self, request, view, obj):
        user = request.user
        if user.is_superuser or user.is_staff:
            return True
        if isinstance(obj, Application):
            return user.company == obj.vacancy.company
        else:
            raise PermissionDenied(
                "Checking owner permission on non-application object"
            )


class AsAdminOrSupervisor(permissions.BasePermission):
    def has_permission(self, request, view):
        return is_admin_or_supervisor(request.user)


class VacancyApprovalPermission(permissions.BasePermission):
    def has_permission(self, request, view):
        return is_admin_or_supervisor(request.user)

    def has_object_permission(self, request, view, obj):
        return isinstance(obj, Vacancy)
