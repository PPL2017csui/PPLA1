from django.shortcuts import render
from django.apps import AppConfig
from kape.settings import RUNNING_DEVSERVER


class CoreConfig(AppConfig):
    name = 'core'


# Create your views here.
def index(request):
    return render(request, 'core/index.html', context={'is_devserver': RUNNING_DEVSERVER})
