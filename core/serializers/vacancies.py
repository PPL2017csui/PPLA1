from rest_framework import serializers

from core.models import Company
from core.models.vacancies import Vacancy, Application
from core.serializers.accounts import StudentSerializer, CompanySerializer


class VacancySerializer(serializers.ModelSerializer):
    company = CompanySerializer()
    status = serializers.SerializerMethodField('_get_app_status')
    bookmarked = serializers.SerializerMethodField('_get_bookmarked_status')

    def _get_app_status(self, instance):
        try:
            request = self.context.get("request")
            student = request.user.student
            app = Application.objects.get(vacancy=instance, student=student)
            return app.status
        except:
            return None
        return None

    def _get_bookmarked_status(self, instance):
        try:
            request = self.context.get("request")
            if request.user.student.bookmarked_vacancies.filter(pk=instance.id).exists():
                return True
            return False
        except:
            return None
        return None

    class Meta:
        model = Vacancy
        fields = ['company', 'verified', 'open_time', 'description', 'close_time', 'created', 'updated', 'name', \
                  'status', 'bookmarked', 'id']


class PostVacancySerializer(serializers.ModelSerializer):
    company = serializers.PrimaryKeyRelatedField(queryset=Company.objects.all())

    class Meta:
        model = Vacancy
        fields = '__all__'


class ApplicationSerializer(serializers.ModelSerializer):
    vacancy = VacancySerializer()
    student = StudentSerializer()

    class Meta:
        model = Application
        fields = '__all__'


class ApplicationStatusSerializer(serializers.ModelSerializer):

    class Meta:
        model = Application
        fields = ['status']

class SupervisorStudentApplicationSerializer(serializers.ModelSerializer):
    def to_representation(self, instance):
        status_map = ["new", "read", "bookmarked", "rejected", "accepted" ]
        return {
            'name' : instance.student.full_name,
            'npm' : instance.student.npm,
            'vacancy_name' : instance.vacancy.name,
            'company_name' : instance.vacancy.company.name,
            'status' : status_map[instance.status]
        }

    class Meta:
        model = Application
        fields = ['name', 'npm', 'vacancy_name', 'company_name', 'status']
        read_only_fields = ['name', 'npm', 'vacancy_name', 'company_name', 'status']

class VacancyApplicationSerializer(serializers.ModelSerializer):
    vacancy = VacancySerializer()

    class Meta:
        model = Application
        fields = ['cover_letter', 'vacancy', 'status']


class VacancyVerifiedSerializer(serializers.ModelSerializer):

    class Meta:
        model = Vacancy
        fields = ['verified']
