from django.contrib.auth.models import User
from rest_framework import serializers

from core.models.accounts import Supervisor, Company, Student
from core.models.vacancies import Application


class BasicUserSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = User
        fields = ('url', 'username', 'email', 'is_staff')


class StudentSerializer(serializers.ModelSerializer):
    user = BasicUserSerializer()
    name = serializers.ReadOnlyField()
    accepted_no = serializers.SerializerMethodField()

    class Meta:
        model = Student
        fields = ['id', 'name', 'user', 'npm', 'resume', 'phone_number', 'birth_place', 'birth_date', 'major', 'batch', \
                  'show_transcript', 'photo', 'accepted_no']

    def get_accepted_no(self, obj):
        apps = Application.objects.filter(student=obj, status=4)
        companies = apps.values('vacancy__company').distinct()
        return companies.count()


class StudentUpdateSerializer(serializers.ModelSerializer):
    email = serializers.EmailField()

    def to_representation(self, instance):
        resume = None
        photo = None
        if instance.resume and hasattr(instance.resume, 'url'):
            resume = instance.resume.url
        if instance.photo and hasattr(instance.photo, 'url'):
            photo = instance.photo.url
        return {
            'resume': resume,
            'email': instance.user.email,
            'phone_number': instance.phone_number,
            'photo': photo,
            'show_transcript': instance.show_transcript
        }

    def update(self, instance, validated_data):
        instance.resume = validated_data.get('resume', instance.resume)
        instance.show_transcript = validated_data.get('show_transcript', instance.show_transcript)
        instance.phone_number = validated_data.get('phone_number', instance.phone_number)
        instance.photo = validated_data.get('photo', instance.photo)
        instance.user.email = validated_data.get('email', instance.user.email)
        instance.save()
        instance.user.save()
        return instance

    class Meta:
        model = Student
        fields = ['resume', 'email', 'phone_number', 'photo', 'show_transcript']


class CompanyUpdateSerializer(serializers.ModelSerializer):
    email = serializers.EmailField()

    def to_representation(self, instance):
        logo = None
        if instance.logo and hasattr(instance.logo, 'url'):
            logo = instance.logo.url
        return {
            'address': instance.address,
            'description': instance.description,
            'email': instance.user.email,
            'logo': logo,
            'name': instance.name
        }

    def update(self, instance, validated_data):
        instance.address = validated_data.get('address', instance.address)
        instance.description = validated_data.get('description', instance.description)
        instance.logo = validated_data.get('logo', instance.logo)
        instance.user.email = validated_data.get('email', instance.user.email)
        instance.user.first_name = validated_data.get('name', instance.user.first_name)
        instance.save()
        instance.user.save()
        return instance

    class Meta:
        model = Company
        fields = ['address', 'description', 'email', 'logo', 'name']


class CompanySerializer(serializers.ModelSerializer):
    user = BasicUserSerializer()
    name = serializers.ReadOnlyField()

    class Meta:
        model = Company
        fields = '__all__'


class SupervisorSerializer(serializers.ModelSerializer):
    user = BasicUserSerializer()
    name = serializers.ReadOnlyField()

    class Meta:
        model = Supervisor
        fields = '__all__'


class UserSerializer(serializers.HyperlinkedModelSerializer):
    company = CompanySerializer()
    supervisor = SupervisorSerializer()
    student = StudentSerializer()

    class Meta:
        model = User
        fields = ('url', 'username', 'email', 'is_staff', 'company', 'supervisor', 'student')


class RegisterSerializer(serializers.HyperlinkedModelSerializer):
    company = CompanySerializer()

    class Meta:
        model = User
        fields = ('url', 'username', 'email', 'is_staff', 'company')
