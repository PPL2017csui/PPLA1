# -*- coding: utf-8 -*-
# Generated by Django 1.10.5 on 2017-03-28 07:17
from __future__ import unicode_literals

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('core', '0003_auto_20170328_1400'),
    ]

    operations = [
        migrations.RenameField(
            model_name='company',
            old_name='alamat',
            new_name='address',
        ),
    ]
