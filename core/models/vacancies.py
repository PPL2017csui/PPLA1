from django.db import models

from core.models.accounts import Company, Student


class Vacancy(models.Model):
    company = models.ForeignKey(Company, related_name="vacancies", null=False)
    verified = models.BooleanField(default=False)
    open_time = models.DateTimeField()
    description = models.TextField(blank=True)
    close_time = models.DateTimeField()
    created = models.DateTimeField(auto_now_add=True)
    updated = models.DateTimeField(auto_now=True)
    name = models.CharField(max_length=100, null=False)

    class Meta:
        ordering = ['-updated']


class Application(models.Model):
    NEW = 0
    READ = 1
    BOOKMARKED = 2
    REJECTED = 3
    ACCEPTED = 4

    cover_letter = models.TextField(null=True, blank=True)
    student = models.ForeignKey(Student, on_delete=models.CASCADE)
    vacancy = models.ForeignKey(Vacancy, on_delete=models.CASCADE)
    status = models.IntegerField(default=NEW)

    class Meta:
        unique_together = (("student", "vacancy"),)

