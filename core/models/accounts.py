import os
import uuid

from django.contrib.auth.models import User
from django.core.validators import MinValueValidator, MaxValueValidator
from django.db import models

from core.lib.validators import validate_document_file_extension, validate_image_file_extension


def get_student_resume_file_path(instance, filename):
    extension = filename.split('.')[-1].lower()
    filename = "%s.%s" % (uuid.uuid4(), extension)
    return os.path.join("student-resume/", filename)


def get_student_photo_file_path(instance, filename):
    extension = filename.split('.')[-1].lower()
    filename = "%s.%s" % (uuid.uuid4(), extension)
    return os.path.join("student-photo/", filename)


def get_company_logo_file_path(instance, filename):
    extension = filename.split('.')[-1].lower()
    filename = "%s.%s" % (uuid.uuid4(), extension)
    return os.path.join("company-logo/", filename)


def get_display_name(user, full_name=False):
    """
    Return a display name that always works like "Benoit J."
    """
    if user.first_name and user.last_name:
        if full_name:
            last = user.last_name.title()
        else:
            last = user.last_name[:1].upper() + "."
        name = user.first_name.title() + " " + last
    elif user.first_name:
        name = user.first_name.title()
    elif user.last_name:
        name = user.last_name.title()
    else:
        name = user.username

    return name.strip()


class Student(models.Model):
    """
    A user subclass
    """
    created = models.DateTimeField(auto_now_add=True)
    updated = models.DateTimeField(auto_now=True)
    user = models.OneToOneField(User)
    npm = models.IntegerField(validators=[MinValueValidator(100000000), MaxValueValidator(9999999999)], unique=True)
    resume = models.FileField(upload_to=get_student_resume_file_path, null=True, blank=True, validators=[validate_document_file_extension])
    phone_number = models.CharField(max_length=100, blank=True, db_index=True, null=True)
    bookmarked_vacancies = models.ManyToManyField('core.Vacancy', related_name="bookmarked_vacancies", blank=True)
    applied_vacancies = models.ManyToManyField('core.Vacancy', related_name="applied_vacancies",
                                               blank=True, through='core.Application')
    birth_place = models.CharField(max_length=30, blank=True, null=True)
    birth_date = models.DateField(blank=True, null=True)
    major = models.CharField(max_length=30, blank=True, null=True)
    batch = models.CharField(max_length=4, blank=True, null=True)
    show_transcript = models.BooleanField(default=False)
    photo = models.FileField(upload_to=get_student_photo_file_path, null=True, blank=True, validators=[validate_image_file_extension])

    @property
    def name(self):
        return get_display_name(self.user)

    @property
    def full_name(self):
        return get_display_name(self.user, full_name=True)

    def __unicode__(self):
        return u"Student {}".format(get_display_name(self.user))

    class Meta:
        ordering = ['-updated']


class Company(models.Model):
    """
    A user subclass
    """
    NEW = 0
    UNVERIFIED = 1
    VERIFIED = 2

    created = models.DateTimeField(auto_now_add=True)
    updated = models.DateTimeField(auto_now=True)
    user = models.OneToOneField(User)
    description = models.TextField()
    status = models.IntegerField(default=NEW)
    logo = models.FileField(upload_to=get_company_logo_file_path, null=True, blank=True,  validators=[validate_image_file_extension])
    address = models.CharField(max_length=1000, blank=True, null=True)

    @property
    def name(self):
        return get_display_name(self.user)

    def __unicode__(self):
        return u"Company {}".format(get_display_name(self.user))

    class Meta:
        ordering = ['-updated']


class Supervisor(models.Model):
    """
    A user subclass
    """
    created = models.DateTimeField(auto_now_add=True)
    updated = models.DateTimeField(auto_now=True)
    user = models.OneToOneField(User)
    nip = models.IntegerField(validators=[MinValueValidator(100000000), MaxValueValidator(9999999999)], unique=True)

    @property
    def name(self):
        return get_display_name(self.user)

    def __unicode__(self):
        return u"Supervisor {}".format(get_display_name(self.user))

    class Meta:
        ordering = ['-updated']
