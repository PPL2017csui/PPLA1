import React from 'react';
import Tabs from './components/Tabs';
import Pane from './components/Pane';
import AdminVacancy from './components/AdminVacancy';
import { Item } from 'semantic-ui-react';


export default class AdminVacancyPage extends React.Component {

  render() {
    return (
      <Tabs selected={0}>
        <Pane label="Lowongan Belum Terferivikasi" >
          <Item.Group>
            <AdminVacancy />
          </Item.Group>
        </Pane>
        <Pane label=" Semua Lamaran" />
      </Tabs>
    );
  }
}
