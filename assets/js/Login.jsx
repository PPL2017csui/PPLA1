import React from 'react';
import { Grid, Segment, Header, Card, Image } from 'semantic-ui-react';
import LoginForm from './components/LoginForm';
import CompanyRegisterModal from './components/CompanyRegisterModal';
import Footer from './components/Footer';

export default class Login extends React.Component {

  static defaultProps = {
    children: null,
  };

  static propTypes = {
    children: React.PropTypes.oneOfType([
      React.PropTypes.arrayOf(React.PropTypes.node),
      React.PropTypes.node,
    ]),
  };

  render = () => (
    <div className="halamanLogin">
      <div className="headerLogin">
        <Header as="h2" icon textAlign="center" >
          <Image src="/assets/img/logo.png" size="medium" centered />
          <Header.Content >
            Kanal Akses Pendaftaran KP Elektronik
          </Header.Content>
        </Header>
      </div>

      <Grid stackable columns={2} padded style={{ display: 'flex', justifyContent: 'center' }}>
        <Grid.Column width="seven">
          <Segment basic>
            <LoginForm type="sso-ui" header="SSO Login" imgSrc="UI.png" imgSize="tiny" />
            {this.props.children}
          </Segment>
        </Grid.Column>

        <Grid.Column width="seven">
          <Segment basic>
            <LoginForm type="company" header="Company Login" imgSrc="logo.png" imgSize="small" usernameLabel="Email"/>
            {this.props.children}
          </Segment >

          <div className="register">
            <Card centered className="register" >
              <Card.Content>
                <Grid columns={2} relaxed>
                  <Grid.Column>
                    <Header as="h3">New to us ?</Header>
                  </Grid.Column>
                  <Grid.Column>
                    <CompanyRegisterModal />
                  </Grid.Column>
                </Grid>
              </Card.Content>
            </Card>
          </div>
        </Grid.Column>
      </Grid>
      <Footer />
    </div>
  )
}

