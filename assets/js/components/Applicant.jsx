import React from 'react';
import { Item, Rating, Grid } from 'semantic-ui-react';
import Server from '../lib/Server';
import ModalAlert from './ModalAlert';
import ApproveModal from './ApproveModal';

const defaultImage = 'https://semantic-ui.com/images/wireframe/image.png';

export default class Applicant extends React.Component {
  static propTypes = {
    data: React.PropTypes.object.isRequired,
    updateStatus: React.PropTypes.func.isRequired,
  };

  static APPLICATION_STATUS = {
    NEW: 0,
    READ: 1,
    BOOKMARKED: 2,
    REJECTED: 3,
    ACCEPTED: 4,
  };

  static APPLICATION_STATUS_TEXT = ['Baru', 'Sudah Dibaca', 'Ditandai', 'Diterima', 'Ditolak'];

  constructor(props) {
    super(props);
    /* istanbul ignore next */
    this.bookmark = this.bookmark.bind(this);
  }

  bookmark = () => {
    let data = { status: Applicant.APPLICATION_STATUS.BOOKMARKED };
    if (this.props.data.status === Applicant.APPLICATION_STATUS.BOOKMARKED) {
      data = { status: Applicant.APPLICATION_STATUS.READ };
    }

    if (this.props.data.status > 2) {
      this.modalAlert.open('Gagal Menandai', 'Lamaran yang sudah ditolak atau diterima tidak bisa ditandai');
    } else {
      Server.patch(`/applications/${this.props.data.id}/`, data).then((status) => {
        this.props.updateStatus(this.props.data.id, status.status);
      });
    }
  };

  render() {
    return (
      <Item >
        <ModalAlert ref={(modal) => { this.modalAlert = modal; }} />
        <Item.Image size="small" src={this.props.data.student.photo || defaultImage} />
        <Item.Content verticalAlign="middle" style={{ wordWrap: 'break-word', width: '100%' }}>
          <Item.Extra>
            <Grid.Row>
              <Grid.Column floated="left">
                <h4> {this.props.data.student.name} </h4>
                {this.props.data.vacancy.name} <br />
                {this.props.data.student.major}
              </Grid.Column>
              <Grid.Column floated="right" textAlign="center">
                <Grid.Row>
                  <Rating
                    icon="star" size="massive"
                    defaultRating={this.props.data.status === Applicant.APPLICATION_STATUS.BOOKMARKED ? 1 : 0}
                    onRate={this.bookmark}
                    maxRating={1}
                  />
                </Grid.Row>
                <ApproveModal updateStatus={this.props.updateStatus} data={this.props.data} />
              </Grid.Column>
            </Grid.Row>
          </Item.Extra>
        </Item.Content>
      </Item>
    );
  }
}
