import React from 'react';
import { Item, Button, Grid, Icon } from 'semantic-ui-react';
import Server from '../lib/Server';
import ModalAlert from './ModalAlert';
import Storage from './../lib/Storage';

const defaultImage = 'https://semantic-ui.com/images/wireframe/image.png';

export default class Company extends React.Component {
  static propTypes = {
    data: React.PropTypes.object.isRequired,
    updateStatus: React.PropTypes.func.isRequired,
  };

  static COMPANY_STATUS = {
    NEW: 0,
    UNVERIFIED: 1,
    VERIFIED: 2,
    ALL: 3,
  };

  constructor(props) {
    super(props);
    /* istanbul ignore next */
    this.state = {
      rejectLoading: false,
      acceptLoading: false,
    };
    this.accept = this.accept.bind(this);
    this.reject = this.reject.bind(this);
    this.goToCompanyHome = this.goToCompanyHome.bind(this);
  }

  reject = () => {
    const data = { status: Company.COMPANY_STATUS.UNVERIFIED };
    this.setState({ rejectLoading: true });
    Server.patch(`/companies/${this.props.data.id}/`, data).then((response) => {
      this.setState({ rejectLoading: false });
      this.props.updateStatus(this.props.data.id, response.status);
    }, () => this.setState({ rejectLoading: false }));
  };

  accept = () => {
    const data = { status: Company.COMPANY_STATUS.VERIFIED };
    this.setState({ acceptLoading: true });
    Server.patch(`/companies/${this.props.data.id}/`, data).then((response) => {
      this.setState({ acceptLoading: false });
      this.props.updateStatus(this.props.data.id, response.status);
    }, () => this.setState({ acceptLoading: false }));
  };

  goToCompanyHome = () => {
    const userData = Storage.get('user-data');
    userData.company = this.props.data;
    Storage.set('user-data', userData);
    const win = window.open('/lowongan');
    win.focus();
  };

  render() {
    return (
      <Item >
        <ModalAlert ref={(modal) => { this.modalAlert = modal; }} />
        <Item.Image size="small" src={this.props.data.logo ? this.props.data.logo : defaultImage} />
        <Item.Content verticalAlign="middle" style={{ wordWrap: 'break-word', width: '100%' }} >
          <Item.Extra>
            <Grid.Row>
              <Grid.Column floated="left" style={{ width: '100%' }}>
                <h4> {this.props.data.name} </h4>
                {this.props.data.address}
              </Grid.Column>
              <Grid.Column floated="right" textAlign="center">
                <div>
                  {this.props.data.status === Company.COMPANY_STATUS.VERIFIED || <Button loading={this.state.acceptLoading} onClick={this.accept} floated="right" color="green" >
                    <Icon name="checkmark" />Terima
                  </Button>}
                  {this.props.data.status === Company.COMPANY_STATUS.UNVERIFIED || <Button loading={this.state.rejectLoading} onClick={this.reject} floated="right" color="red" >
                    <Icon name="remove" />Tolak
                  </Button>}
                  <Button onClick={this.goToCompanyHome} floated="right" color="facebook" >
                    <Icon name="home" />Login
                  </Button>
                </div>
              </Grid.Column>
            </Grid.Row>
          </Item.Extra>
        </Item.Content>
      </Item>
    );
  }
}
