import React from 'react';
import { browserHistory } from 'react-router';
import { Modal, Button, Form, Input, TextArea, Header, Icon } from 'semantic-ui-react';
import ModalAlert from './../components/ModalAlert';
import Server from './../lib/Server';
import Storage from './../lib/Storage';
import Dumper from './../lib/Dumper';

export default class CompanyRegisterModal extends React.Component {

  constructor(props) {
    super(props);
    /* istanbul ignore next */
    this.state = { loading: false };
    this.handleChange = this.handleChange.bind(this);
    this.handleFile = this.handleFile.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
  }

  componentWillUpdate() {
    this.fixBody();
  }

  componentDidUpdate() {
    this.fixBody();
  }

  fixBody = () => {
    const anotherModal = document.getElementsByClassName('ui page modals').length;
    if (anotherModal > 0) document.body.classList.add('scrolling', 'dimmable', 'dimmed');
  };

  handleChange = (e) => {
    this.setState({ [e.target.name]: e.target.value });
  };

  handleFile = (e) => {
    this.setState({ [e.target.name]: e.target.files[0] });
  };


  handlePassword = (e) => {
    if (e.target.name === 'password') this.passwordField = e.target; else
    if (e.target.name === 'password-confirm') this.passwordConfirmField = e.target;
    const isExist = this.passwordField && this.passwordConfirmField;
    if (isExist) {
      if (this.passwordField.value !== this.passwordConfirmField.value) {
        this.passwordConfirmField.setCustomValidity("Passwords Don't Match");
      } else {
        this.passwordConfirmField.setCustomValidity('');
      }
    }
  };

  handleSubmit = (e) => {
    e.preventDefault();
    this.setState({ loading: true });
    Server.submit('/register/', this.state).then((response) => {
      Storage.set('user-data', response);
      browserHistory.push('/home');
    }, error => error.then((r) => {
      this.setState({ loading: false });
      this.modalAlert.open('Gagal Membuat Akun', Dumper.dump(r));
    }));
  };

  render = () => (
    <Modal trigger={<Button primary floated="right">Register</Button>} closeIcon="close">
      <Header icon="archive" content="Register for More Benefits" />
      <Modal.Content>
        <ModalAlert ref={(modal) => { this.modalAlert = modal; }} />
        <Form onSubmit={this.handleSubmit}>
          <Header as="h2" icon textAlign="center">
            <Icon name="signup" circular />
            <Header.Content>
                        Register
            </Header.Content>
          </Header>
          <Form.Field required>
            <label htmlFor="email">Email</label>
            <Input onChange={this.handleChange} type="email" name="email" icon="user" iconPosition="left" placeholder="email" required />
          </Form.Field>
          <Form.Field required>
            <label htmlFor="password">Password</label>
            <Input
              onChange={(e) => { this.handleChange(e); this.handlePassword(e); }}
              type="password" id="password" name="password" icon="key" iconPosition="left" placeholder="password" required
            />
          </Form.Field>
          <Form.Field required>
            <label htmlFor="password-confirm">Konfirmasi Password</label>
            <Input
              onChange={(e) => { this.handleChange(e); this.handlePassword(e); }}
              type="password" id="password-confirm" name="password-confirm" icon="key" iconPosition="left" placeholder="password" required
            />
          </Form.Field>

          <Form.Field required>
            <label htmlFor="name">Nama Perusahaan</label>
            <Input onChange={this.handleChange} placeholder="Nama Perusahaan" name="name" required />
          </Form.Field>
          <Form.Field required>
            <label htmlFor="logo">Logo</label>
            <Input
              onChange={this.handleFile}
              name="logo"
              icon={{ name: 'attach', circular: true, link: true }}
              placeholder="attach logo"
              type="File"
              required
            />
          </Form.Field>
          <Form.Field required>
            <label htmlFor="description">Deskripsi</label>
            <TextArea onChange={this.handleChange} placeholder="Tell us more" name="description" autoHeight required />
          </Form.Field>
          <Form.Field required>
            <label htmlFor="address">Alamat</label>
            <Input onChange={this.handleChange} placeholder="Alamat" name="address" required />
          </Form.Field>
          <Modal.Actions style={{ textAlign: 'right' }}>
            <Button loading={this.state.loading} type="submit" color="blue"> <Icon name="checkmark" />Submit</Button>
          </Modal.Actions>
        </Form>
      </Modal.Content>
    </Modal>
  )
}

