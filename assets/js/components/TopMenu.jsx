import React from 'react';
import { Menu, Image, Popup, Button, Card } from 'semantic-ui-react';
import { Link, browserHistory } from 'react-router';
import Server from '../lib/Server';
import Storage from '../lib/Storage';

const defaultPicture = 'https://semantic-ui.com/images/avatar/small/elliot.jpg';

export default class TopMenu extends React.Component {

  static getInfo(user) {
    const adminRole = {
      name: 'admin',
      user: {
        email: '',
      },
      photo: null,
    };
    const role = user.role;
    if (role === 'student') {
      return user.data.student;
    } else if (role === 'company') {
      return user.data.company;
    } else if (role === 'supervisor') {
      return user.data.supervisor;
    }

    return adminRole;
  }

  static propTypes = {
    user: React.PropTypes.object.isRequired,
  };

  constructor(props) {
    super(props);
    /* istanbul ignore next */
    this.state = { activeItem: 'Beranda', logoutLoading: false };
    this.logout = this.logout.bind(this);
    this.logoutCompany = this.logoutCompany.bind(this);
  }

  handleItemClick = (e, { name }) => this.setState({ activeItem: name });

  logout = (e) => {
    e.preventDefault();
    this.setState({ logoutLoading: true });
    Server.get('/api-auth/logout/?next=/', true).then(() => {
      Storage.clear();
      browserHistory.push('/login');
    }, () => this.setState({ logoutLoading: false }));
  };

  logoutCompany = /* istanbul ignore next */ (e) => {
    e.preventDefault();
    const userData = Storage.get('user-data');
    userData.company = null;
    Storage.set('user-data', userData);
    window.location.replace('/home');
  };

  render() {
    const { activeItem } = this.state;
    const data = TopMenu.getInfo(this.props.user);
    return (
      <div>
        { this.props.user.data.is_staff && this.props.user.data.company && (
          <div className="admin-bar">
            <p>
              Anda login sebagai perusahaan:
              <b> {this.props.user.data.company.name}</b> (#{this.props.user.data.company.id}).
              Untuk kembali menjadi admin, klik <a href="#" onClick={this.logoutCompany}> link ini</a>
            </p>
          </div>
        )}
        <Menu color="blue" pointing secondary>
          <Image as={Link} size="small" src="/assets/img/logo.png" to="/home" />
          <Menu.Menu position="right">
            <Menu.Item as={Link} to="/home" name="Beranda" active={activeItem === 'Beranda'} onClick={this.handleItemClick} />
            { this.props.user.role === 'student' &&
            <Menu.Item as={Link} to="/profil-mahasiswa" name="Profil" active={activeItem === 'Profil'} onClick={this.handleItemClick} /> }
            { this.props.user.role === 'company' &&
            <Menu.Item as={Link} to="/profil-perusahaan" name="Profil" active={activeItem === 'Profil'} onClick={this.handleItemClick} /> }
            { this.props.user.data.is_staff &&
            <Menu.Item as={Link} to="/lowongan" name="Lowongan" active={activeItem === 'Lowongan'} onClick={this.handleItemClick} /> }
            <Menu.Item style={{ padding: '10px 0' }}>
              <Popup
                trigger={<Image src={(this.props.user.role === 'company' ? data.logo : data.photo) || defaultPicture} avatar />}
                flowing hoverable
              >
                <Card
                  header={data.name}
                  description={data.user.email}
                />
                <Button as={Link} onClick={this.logout} loading={this.state.logoutLoading} name="logout" color="blue" size="tiny">Keluar</Button>
              </Popup>
            </Menu.Item>
          </Menu.Menu>
        </Menu>
      </div>
    );
  }
}
