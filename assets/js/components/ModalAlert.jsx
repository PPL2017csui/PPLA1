import React from 'react';
import { Button, Header, Icon, Modal } from 'semantic-ui-react';

export default class ModalAlert extends React.Component {
  constructor(props) {
    super(props);
    /* istanbul ignore next */
    this.state = { open: false, header: '', content: '', callback: () => {} };
    this.open = this.open.bind(this);
    this.close = this.close.bind(this);
  }

  componentWillUpdate() {
    this.fixBody();
  }

  componentDidUpdate() {
    this.fixBody();
  }

  fixBody = () => {
    const anotherModal = document.getElementsByClassName('ui page modals').length;
    if (anotherModal > 0) document.body.classList.add('scrolling', 'dimmable', 'dimmed');
  };

  open = (header = this.state.header, content = this.state.content, callback = this.state.callback) => {
    this.setState({ open: true, header, content, callback });
  };

  close = () => {
    this.setState({ open: false });
    this.state.callback();
  };

  render = () => {
    const style = { whiteSpace: 'pre-wrap' };
    return (
      <Modal open={this.state.open} basic size="small">
        <Header icon="warning sign" content={this.state.header} />
        <Modal.Content>
          <p style={style}>{this.state.content}</p>
        </Modal.Content>
        <Modal.Actions>
          <Button color="green" inverted onClick={this.close}>
            <Icon name="checkmark" /> OK
          </Button>
        </Modal.Actions>
      </Modal>
    );
  };
}
