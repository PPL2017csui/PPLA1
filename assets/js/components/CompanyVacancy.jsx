import React from 'react';
import moment from 'moment';
import { Button, Icon, Item, Grid } from 'semantic-ui-react';
import { Link } from 'react-router';
import Server from '../lib/Server';

const defaultImage = 'https://semantic-ui.com/images/wireframe/image.png';

export default class CompanyVacancy extends React.Component {
  static propTypes = {
    data: React.PropTypes.object.isRequired,
    deleteCallback: React.PropTypes.func,
  };

  static defaultProps = { deleteCallback: () => {} };

  constructor(props) {
    super(props);
    /* istanbul ignore next */
    moment.locale('id');
    this.state = { deleteLoading: false, count: 0, countNew: 0 };
    Server.get(`/vacancies/${this.props.data.id}/count/`, false).then((data) => {
      this.setState({ count: data.count, countNew: data.count_new });
    });
  }

  getLink = `/buat-lowongan/${this.props.data.id}`;

  render() {
    return (

      <Item className="applicantItems">
        <Item.Image src={this.props.data.company.logo ? this.props.data.company.logo : defaultImage} size="small" />
        <Item.Content verticalAlign="middle" style={{ wordWrap: 'break-word', width: '100%' }} >
          <Item.Header as="a">{this.props.data.name}</Item.Header>
          <Grid.Row>
            <Grid.Column floated="left">
              <p>{ this.state.count } Pendaftar<br/>
              { this.state.countNew } Pendaftar Baru<br/><br/>
              Ditutup {moment(moment(this.props.data.close_time)).fromNow()}</p>
            </Grid.Column>
            <Grid.Column floated="right">
              {this.props.data.verified ?
                (<h4> <Icon name="checkmark box" size="large" color="green" /> Terverifikasi </h4>) :
                (<h4> <Icon name="remove circle" size="large" color="red" /> Belum Terverifikasi </h4>)}
              <Button color="blue" floated="right" as={Link} to={this.getLink}>
                Ubah <Icon name="right chevron" />
              </Button>
              <Button loading={this.state.deleteLoading} color="red" floated="right" onClick={() => { this.setState({ deleteLoading: true }); this.props.deleteCallback(); }} >
                Hapus <Icon name="delete" />
              </Button>
            </Grid.Column>
          </Grid.Row>
        </Item.Content>
      </Item>
    );
  }
}
