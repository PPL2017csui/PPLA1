import React from 'react';
import { Modal, Button, Icon, Segment } from 'semantic-ui-react';
import Server from './../lib/Server';
import ConfirmationModal from './../components/ConfirmationModal';
import Applicant from './../components/Applicant';

export default class ApproveModal extends React.Component {
  static propTypes = {
    data: React.PropTypes.object.isRequired,
    updateStatus: React.PropTypes.func.isRequired,
  };

  constructor(props) {
    super(props);
    /* istanbul ignore next */
    this.state = {
      modalOpen: false,
      rejectLoading: false,
      acceptLoading: false,
    };
    this.handleOpen = this.handleOpen.bind(this);
    this.reject = this.reject.bind(this);
    this.accept = this.accept.bind(this);
    this.gotoStudentProfile = this.gotoStudentProfile.bind(this);
    this.gotoStudentResume = this.gotoStudentResume.bind(this);
    this.gotoStudentTranscript = this.gotoStudentTranscript.bind(this);
  }

  componentWillUpdate() {
    this.fixBody();
  }

  componentDidUpdate() {
    this.fixBody();
  }

  fixBody = () => {
    const anotherModal = document.getElementsByClassName('ui page modals').length;
    if (anotherModal > 0) document.body.classList.add('scrolling', 'dimmable', 'dimmed');
  };

  handleOpen = () => this.setState({ modalOpen: true });
  handleClose = () => { this.readApplication(); this.setState({ modalOpen: false }); }

  readApplication = () => {
    const data = { status: Applicant.APPLICATION_STATUS.READ };
    return this.props.data.status === Applicant.APPLICATION_STATUS.NEW && Server.patch(`/applications/${this.props.data.id}/`, data).then((status) => {
      this.props.updateStatus(this.props.data.id, status.status);
    });
  };

  rejectApplication = () => {
    const data = { status: Applicant.APPLICATION_STATUS.REJECTED };
    this.setState({ rejectLoading: true });
    Server.patch(`/applications/${this.props.data.id}/`, data).then((status) => {
      this.props.updateStatus(this.props.data.id, status.status);
    });
  };

  reject = () => {
    this.modal.open(
      'Tolak Lamaran?',
      'Apakah anda yakin untuk menolak lamaran ini?',
      'trash',
      this.rejectApplication,
    );
  };

  acceptApplication = () => {
    const data = { status: Applicant.APPLICATION_STATUS.ACCEPTED };
    this.setState({ acceptLoading: true });
    Server.patch(`/applications/${this.props.data.id}/`, data).then((status) => {
      this.props.updateStatus(this.props.data.id, status.status);
    });
  };

  gotoLink = (url) => {
    const win = window.open(url);
    win.focus();
  };

  gotoStudentResume = () => this.gotoLink(this.props.data.student.resume);

  gotoStudentTranscript = () => this.gotoLink(`/transkrip/${this.props.data.id}`);

  gotoStudentProfile = () => this.gotoLink(`/mahasiswa/${this.props.data.student.id}`);

  accept = () => {
    this.modal.open(
      'Terima Lamaran?',
      'Apakah anda yakin untuk menerima lamaran ini?',
      'checkmark',
      this.acceptApplication,
    );
  };

  render = () => (
    <Modal
      trigger={<Button primary onClick={this.handleOpen} floated="right">Detail</Button>}
      closeIcon="close"
      open={this.state.modalOpen}
      onClose={this.handleClose}
    >
      <ConfirmationModal ref={(modal) => { this.modal = modal; }} />
      <Modal.Header>Data Lamaran</Modal.Header>
      <Modal.Content>
        <h4> Cover Letter </h4>
        <Segment>
          <p>
            { this.props.data.cover_letter ? this.props.data.cover_letter : 'Kosong' }
          </p>
        </Segment>
        <br />
        <div>
          <b>
            {this.props.data.student.resume ? <a onClick={this.gotoStudentResume} href="#" >CV Pelamar </a> : 'Pelamar tidak memiliki CV'}
            <br />
            {this.props.data.student.show_transcript ? <a onClick={this.gotoStudentTranscript} href="#" >Transkrip Pelamar</a> : 'Pelamar tidak mengijinkan transkrip dilihat'}
            <br />
          </b>
        </div>
      </Modal.Content>
      <Modal.Actions>
        <Button color="facebook" onClick={this.gotoStudentProfile} floated="left" >
          <Icon name="user outline" /> Lihat Profil
        </Button>
        <Button.Group>
          <Button disabled={this.props.data.status === Applicant.APPLICATION_STATUS.REJECTED} loading={this.state.rejectLoading} color="red" onClick={this.reject}>Tolak Lamaran</Button>
          <Button.Or />
          <Button disabled={this.props.data.status === Applicant.APPLICATION_STATUS.ACCEPTED} loading={this.state.acceptLoading} color="green" onClick={this.accept}>Terima Lamaran</Button>
        </Button.Group>
      </Modal.Actions>
    </Modal>
    )
}

