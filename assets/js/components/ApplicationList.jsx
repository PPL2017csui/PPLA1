import React from 'react';
import { Table } from 'semantic-ui-react';

export default class ApplicationList extends React.Component {
  static propTypes = {
    cols: React.PropTypes.any.isRequired,
    items: React.PropTypes.any,
  };

  static defaultProps = {
    items: [],
  };

  generateHeaders() {
    const cols2 = this.props.cols;  // [{key, label}]

        // generate our header (th) cell components
    return cols2.map(colData =>
      <Table.HeaderCell singleLine key={colData.key}> {colData.label} </Table.HeaderCell>
     );
  }

  generateRows() {
    return this.props.items.map(item =>
      (
        <Table.Row key={`${item.npm}_${item.company}_${item.position}_${item.status}_row`}>
          <Table.Cell key={`${item.name}_name`}> {item.name} </Table.Cell>
          <Table.Cell key={`${item.name}_npm`}> {item.npm} </Table.Cell>
          <Table.Cell key={`${item.name}_company`}>
            {item.company_name}
          </Table.Cell>
          <Table.Cell key={`${item.name}_position`}>
            {item.vacancy_name}
          </Table.Cell>
          <Table.Cell key={`${item.name}_status`}>
            {item.status}
          </Table.Cell>
        </Table.Row>
      )
    );
  }

  render() {
    return (
      <Table celled padded>
        <Table.Header >
          <Table.Row>
            {this.generateHeaders()}
          </Table.Row>
        </Table.Header>
        <Table.Body>{this.generateRows()}</Table.Body>
      </Table>
    );
  }
}
