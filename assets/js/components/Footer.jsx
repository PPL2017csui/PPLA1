import React from 'react';
import { Container } from 'semantic-ui-react';

export default class Footer extends React.Component {
  render() {
    return (
      <div className="footer">
        <Container textAlign="center">
          <h5>All Rights Reserved © 2017</h5>
        </Container>
      </div>
    );
  }
}
