import React from 'react';
import { Segment, Image, Header, Icon, Checkbox, Container, Button, Form, Grid } from 'semantic-ui-react';
import Server from './lib/Server';
import Storage from './lib/Storage';
import ModalAlert from './components/ModalAlert';
import Dumper from './lib/Dumper';

export default class ProfilePage extends React.Component {

  static propTypes = {
    route: React.PropTypes.object.isRequired,
    params: React.PropTypes.object.isRequired,
    user: React.PropTypes.object.isRequired,
  };

  constructor(props) {
    super(props);
    /* istanbul ignore next */
    this.state = {
      id: '',
      npm: '',
      name: '',
      major: '',
      batch: '',
      email: '',
      cityOfBirth: '',
      dateOfBirth: '',
      resume: '',
      phone_number: '',
      show_transcript: '',
      photo: '',
      form: {
        picture: '',
        email: '',
        phone_number: '',
        resume: '',
        show_transcript: '',
      },
      bagikanTranskrip: '',
      acceptedNo: 0,
      refresh: 1,
      loading: false,
    };
    this.getProfile = this.getProfile.bind(this);
    this.handleChange = this.handleChange.bind(this);
    this.handleCheckbox = this.handleCheckbox.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
    this.handleFile = this.handleFile.bind(this);
    this.gotoLink = this.gotoLink.bind(this);
    this.gotoStudentResume = this.gotoStudentResume.bind(this);
    this.gotoStudentTranscript = this.gotoStudentTranscript.bind(this);
    this.getProfile();
  }

  getProfile() {
    const id = this.props.route.own ? this.props.user.data.student.id : this.props.params.id;
    return Server.get(`/students/${id}/`).then((data) => {
      this.setState({
        id: data.id,
        name: data.name,
        npm: data.npm,
        resume: data.resume,
        major: data.major,
        batch: data.batch,
        email: data.user.email,
        cityOfBirth: data.birth_place,
        dateOfBirth: data.birth_date,
        phone_number: data.phone_number,
        photo: data.photo,
        show_transcript: data.show_transcript,
        acceptedNo: data.accepted_no,
        bagikanTranskrip: data.show_transcript,
        refresh: this.state.refresh + 1,
      });
      if (this.props.route.own) {
        const newSession = this.props.user.data;
        newSession.student = data;
        Storage.set('user-data', newSession);
        window.scrollTo(0, 0);
      }
    }, error => error.then(() => {
      this.state.name = 'Gagal mendapatkan informasi';
    }));
  }

  handleSubmit = (e) => {
    e.preventDefault();

    const submitForm = {};
    Object.keys(this.state.form).forEach((key) => {
      if (this.state.form[key] !== '') {
        submitForm[key] = this.state.form[key];
      }
    });
    this.setState({ loading: true });
    Server.submit(`/students/${this.state.id}/profile/`, submitForm, 'PATCH').then(() => {
      this.setState({ loading: false });
      this.modalAlert.open('Profil berhasil diperbaharui', 'Silakan periksa kembali profil anda', this.getProfile);
    }, error => error.then((r) => {
      this.setState({ loading: false });
      this.modalAlert.open('Pembaharuan profil gagal', Dumper.dump(r));
    }));
  };


  handleFile = (e) => {
    const form = this.state.form;
    form[e.target.name] = e.target.files[0];
    this.setState({ form });
  };

  handleChange = (e) => {
    const form = this.state.form;
    form[e.target.name] = e.target.value;
    this.setState({ form });
  };

  handleCheckbox = (e, d) => {
    const form = this.state.form;
    form[d.name] = d.checked;
    this.setState({ form, show_transcript: d.checked });
  };

  gotoLink = (url) => {
    const win = window.open(url);
    win.focus();
  };

  gotoStudentResume = () => this.gotoLink(this.state.resume || '#');

  gotoStudentTranscript = () => this.gotoLink(`transkrip/${this.state.id}`);

  updateForm(show) {
    if (show) {
      return (
        <Segment className="profile-form">
          <Header as="h3" textAlign="center">
            <Icon name="edit" />
            <Header.Content>
              Edit Profile Page
            </Header.Content>
          </Header>
          <ModalAlert ref={(modal) => { this.modalAlert = modal; }} />
          <Form ref={(input) => { this.form = input; }} key={this.state.refresh} size="small" onSubmit={this.handleSubmit}>
            <Form.Field>
              <label htmlFor="photo">Profile Picture</label>
              <input onChange={this.handleFile} placeholder="Profile Photo.jpg" name="photo" type="File" />
            </Form.Field>
            <Form.Field>
              <label htmlFor="email">Email</label>
              <input onChange={this.handleChange} placeholder="jojon@email.com" name="email" />
            </Form.Field>
            <Form.Field>
              <label htmlFor="phone_number">No. Hp</label>
              <input onChange={this.handleChange} placeholder="08123456789" name="phone_number" />
            </Form.Field>
            <Form.Field>
              <label htmlFor="resume">Resume</label>
              <input onChange={this.handleFile} placeholder="Resume" name="resume" type="File" />
            </Form.Field>
            <Form.Field>
              <Checkbox
                onChange={this.handleCheckbox}
                checked={!!this.state.show_transcript}
                label="Ijinkan perusahaan tempat saya mendaftar untuk melihat transkrip akademik saya"
                name="show_transcript"
              />
            </Form.Field>
            <Button type="submit" size="small" loading={this.state.loading} primary floated="right">Submit</Button>
          </Form>
        </Segment>
      );
    }

    return (<div />);
  }

  render() {
    const defaultPicture = 'https://semantic-ui.com/images/wireframe/square-image.png';
    return (
      <div className="profilePage">
        <Segment className="biodata-section" >
          <Grid stackable columns={2}>
            <Grid.Column>
              <Header as="h2" icon textAlign="center">
                <br />
                <Image src={this.state.photo || defaultPicture} size="medium" />
              </Header>
            </Grid.Column>

            <Grid.Column>
              <Container textAlign="left" className="profile-biodata">
                <div className="biodata">
                  <Segment basic textAlign="center">
                    <h1> { this.state.name } </h1>
                  </Segment>

                  <Segment basic vertical>
                    <Grid>
                      <Grid.Column width={2}>
                        <Icon name="university" size="big" />
                      </Grid.Column>
                      <Grid.Column width={13}>
                        <p> { this.state.major || 'N/A' }, { this.state.batch || 'N/A' } </p>
                      </Grid.Column>
                    </Grid>
                  </Segment>

                  <Segment basic vertical>

                    <Grid>
                      <Grid.Column width={2}>
                        <Icon name="mail" size="big" />
                      </Grid.Column>
                      <Grid.Column width={13}>
                        <p> { this.state.email || 'N/A' } </p>
                      </Grid.Column>
                    </Grid>
                  </Segment>

                  <Segment basic vertical>
                    <Grid>
                      <Grid.Column width={2}>
                        <Icon name="phone" size="big" />
                      </Grid.Column>
                      <Grid.Column width={13}>
                        <p> { this.state.phone_number || 'N/A' }</p>
                      </Grid.Column>
                    </Grid>
                  </Segment>

                  <Segment basic vertical>
                    <Grid>
                      <Grid.Column width={2}>
                        <Icon name="gift" size="big" />
                      </Grid.Column>
                      <Grid.Column width={13}>
                        <p> { this.state.cityOfBirth || 'N/A' }, { this.state.dateOfBirth || 'N/A' } </p>
                      </Grid.Column>
                    </Grid>
                  </Segment>
                </div>

                <Container textAlign="center">
                  <div className="buttonProfile">
                    <Button onClick={this.gotoStudentResume} disabled={!this.state.resume} primary size="small">Resume</Button>
                    <Button onClick={this.gotoStudentTranscript} color="green" size="small">Transkrip</Button>
                  </div>
                  <br />
                  <div>
                    <h4> Bagikan Transkrip : { this.state.bagikanTranskrip ? 'Ya' : 'Tidak' }</h4>
                  </div>
                </Container>
              </Container>
            </Grid.Column >
          </Grid>
        </Segment >
        { this.updateForm(this.props.route.own) }
      </div>
    );
  }
}
