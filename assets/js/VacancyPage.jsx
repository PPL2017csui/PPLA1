import React from 'react';
import { Segment, Button } from 'semantic-ui-react';
import { Link } from 'react-router';
import Tabs from './components/Tabs';
import Pane from './components/Pane';
import VacancyList from './components/VacancyList';
import Pagination from './components/Pagination';

export default class VacancyPage extends React.Component {

  static propTypes = {
    user: React.PropTypes.object.isRequired,
  };

  static getId(user) {
    const role = user.role;
    if (role === 'student') {
      return user.data.student.id;
    } else if (role === 'company' || (role === 'admin' && user.data.company != null)) {
      return user.data.company.id;
    } else if (role === 'supervisor' || (role === 'admin' && user.data.supervisor != null)) {
      return user.data.supervisor.id;
    }

    return 0;
  }

  constructor(props) {
    super(props);
    /* istanbul ignore next */
    this.state = {
      vacancies: [],
      id: VacancyPage.getId(this.props.user),
    };
  }


  generateVacancies() {
    if (this.props.user.role === 'student') {
      return (
        <Tabs selected={0}>
          <Pane label="Lowongan" >
            <Pagination
              key={1}
              url="/vacancies/"
              child={
                <VacancyList
                  user={this.props.user}
                  key={1}
                  userId={this.state.id}
                />
              }
            />
          </Pane>
          <Pane label="Lamaran Saya" >
            <Pagination
              key={2}
              url={`/students/${this.state.id}/applied-vacancies/`}
              child={
                <VacancyList
                  user={this.props.user}
                  key={2}
                  userId={this.state.id}
                />
              }
            />
          </Pane>
          <Pane label="Ditandai" >
            <Pagination
              key={3}
              url={`/students/${this.state.id}/bookmarked-vacancies/`}
              child={
                <VacancyList
                  key={3}
                  user={this.props.user}
                  userId={this.state.id}
                />
              }
            />
          </Pane>
        </Tabs>
      );
    } else if ((this.props.user.role === 'admin' && this.props.user.data.company != null)
      || this.props.user.role === 'company') {
      return (
        <Segment className="paginationCompany">
          <Pagination
            key={1}
            url={`/companies/${this.state.id}/vacancies/`}
            child={
              <VacancyList
                key={1}
                user={this.props.user}
                userId={this.state.id}
              />
            }
            error="Akun anda belum terverifikasi. Harap hubungi pihak administrasi"
          />
        </Segment>
      );
    } else if (this.props.user.role === 'admin' || this.props.user.role === 'supervisor') {
      return (
        <Tabs selected={0}>
          <Pane label="Lowongan Belum Terverifikasi" >
            <Pagination
              key={1}
              url="/vacancies/?verified=false"
              child={
                <VacancyList
                  user={this.props.user}
                  key={1}
                  userId={this.state.id}
                />
              }
            />
          </Pane>
          <Pane label="Lowongan Terverifikasi" >
            <Pagination
              key={2}
              url="/vacancies/?verified=true"
              child={
                <VacancyList
                  user={this.props.user}
                  key={2}
                  userId={this.state.id}
                />
              }
            />
          </Pane>
        </Tabs>
      );
    }

    return (
      <div>
        <h3>
          Anda tidak terautentifikasi. Harap logout dan login
          kembali dengan akun yang benar
        </h3>
      </div>
    );
  }

  companyHeader() {
    if ((this.props.user.role === 'admin' && this.props.user.data.company != null) || this.props.user.role === 'company') {
      return (
        <div style={{ float: 'left' }}>
          <Button as={Link} to="/pelamar" icon="eye" labelPosition="left" color="facebook" content="Lihat Semua Pendaftar" />
          <Button as={Link} to="/buat-lowongan" icon="add" labelPosition="left" content="Tambah Lowongan Baru" color="teal" />
        </div>
      );
    }

    return '';
  }

  render() {
    return (
      <div className="applicant">
        {this.props.user.role === 'student' || (
          <div className="administrationButtons" style={{ display: 'inline-block' }}>
            { this.companyHeader() }
            {(this.props.user.role === 'admin' || this.props.user.role === 'supervisor') &&
            <Button
              as={Link} to="/rekap" icon="dashboard" labelPosition="left" color="facebook"
              content="Rekap Pendaftaran"
            />}
          </div>
        )}
        { this.generateVacancies() }
      </div>
    );
  }
}
