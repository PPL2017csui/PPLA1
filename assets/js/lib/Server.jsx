import Logger from './Logger';
import Storage from './Storage';

export default class Server {
  static getCookie(name) {
    if (document.cookie && document.cookie !== '') {
      const cookies = document.cookie.split(';');
      for (let i = 0; i < cookies.length; i += 1) {
        const cookie = cookies[i].trim();
        if (cookie.substring(0, name.length) === (`${name}`)) {
          // Does this cookie string begin with the name we want?
          /* istanbul ignore next */
          return decodeURIComponent(cookie.substring(name.length + 1));
        }
      }
    }
    return null;
  }

  static submit(path, data, method = 'POST', useCache = false) {
    const form = new FormData();
    Object.keys(data).map(k => {
      form.append(k, data[k]);
    });

    const requestData = {
      headers: { 'X-CSRFToken': Server.getCookie('csrftoken') },
      body: form,
      credentials: 'same-origin',
      method,
    };

    return this.handleFetchRequest(requestData, path, useCache);
  }

  static sendRequest(path, method, data, useCache = false) {
    const csrftoken = this.getCookie('csrftoken');
    const headers = {
      'content-type': 'application/json',
      Accept: 'application/json',
      'X-CSRFToken': csrftoken,
    };
    const requestData = {
      headers,
      method,
      body: data !== null ? JSON.stringify(data) : undefined,
      credentials: 'same-origin',
    };

    return this.handleFetchRequest(requestData, path, useCache);
  }

  static handleFetchRequest(requestData, path, useCache) {
    const url = `/api${path}`;
    const request = fetch(url, requestData);

    /* istanbul ignore next */
    const promise = request.then((response) => {
      if (response.status === 204) {
        return response;
      }
      const contentType = response.headers.get('content-type');
      const result = contentType && contentType.indexOf('application/json') !== -1 ? response.json() : response.text();

      if (response.status < 200 || response.status > 399) throw result;
      else return result;
    });

    /* istanbul ignore next */
    return useCache ? promise.then((response) => {
      Logger.log('[Server] Response from', url, response);
      Storage.set(path, response);
      return response;
    }) : promise.then((response) => {
      Logger.log('[Server] Response from', url, response);
      return response;
    });
  }

  static get(path, useCache = false) {
    return (useCache && Storage.get(path)) ?
            Promise.resolve(Storage.get(path)) : this.sendRequest(path, 'GET', null, useCache);
  }

  static post(path, data) {
    return this.sendRequest(path, 'POST', data);
  }

  static patch(path, data) {
    return this.sendRequest(path, 'PATCH', data);
  }

  //    noinspection ReservedWordAsName
  static delete(path) {
    return this.sendRequest(path, 'DELETE', {});
  }

  static request(url, method, payload, success, error) {
    const request = new XMLHttpRequest();
    request.open(method, url, true);
    request.onload = success;
    request.onerror = error;
    request.setRequestHeader('Content-Type', 'text/plain');
    return request.send(payload);
  }

  static isLoggedIn() {
    return this.getCookie('sessionid');
  }
}
