import React from 'react';
import TopMenu from './components/TopMenu';
import Footer from './components/Footer';

export default class Dashboard extends React.Component {
  static propTypes = {
    user: React.PropTypes.object.isRequired,
    route: React.PropTypes.object.isRequired,
    params: React.PropTypes.object.isRequired,
    children: React.PropTypes.oneOfType([
      React.PropTypes.arrayOf(React.PropTypes.node),
      React.PropTypes.node,
    ]).isRequired,
  };

  constructor(props) {
    super(props);
    /* istanbul ignore next */
  }

  render = () => (
    <div>
      <TopMenu user={this.props.user} />
      <div className="mainContent">
        {this.props.children}
      </div>
      <Footer />
    </div>
  )
}

