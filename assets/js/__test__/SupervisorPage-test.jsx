import React from 'react';
import ReactTestUtils from 'react-addons-test-utils';
import fetchMock from 'fetch-mock';
import SupervisorPage from '../SupervisorPage';

describe('SupervisorPage', () => {
  const data = {
    count: 5,
    next: null,
    previous: null,
    results: [
      {
        company_name: 'Tutuplapak',
        name: 'Joshua Casey Darian Gunawan',
        npm: 1406622616,
        vacancy_name: 'Software Engineer',
        status: 'accepted',
      },
      {
        company_name: 'Tutuplapak',
        name: 'Muhammad Reza Qorib',
        npm: 1406543593,
        vacancy_name: 'Software Engineer',
        status: 'accepted',
      },
      {
        company_name: 'Tutuplapak',
        name: 'Muhammad Reza Qorib',
        npm: 1406543593,
        vacancy_name: 'Kepala Sekolah',
        status: 'read',
      },
      {
        company_name: 'company1',
        name: 'Farhan Farasdak',
        npm: 1406572321,
        vacancy_name: 'Data Scientist',
        status: 'new',
      },
      {
        company_name: 'company1',
        name: 'student2',
        npm: 1406527513,
        vacancy_name: 'Data Scientist',
        status: 'new',
      },
    ],
  };

  fetchMock.get('*', data);

  it('renders for admin without problem', () => {
    const supervisorPage = ReactTestUtils.renderIntoDocument(
      <SupervisorPage />);
    expect(supervisorPage).to.exist;
    fetchMock.restore();
  });
});
