import React from 'react';
import ReactTestUtils from 'react-addons-test-utils';
import Login from '../Login';

describe('Login', () => {
  it('renders for login without problem', () => {
    const login = ReactTestUtils.renderIntoDocument(<Login><div>test</div></Login>);
    expect(login).to.exist;
  });
});

