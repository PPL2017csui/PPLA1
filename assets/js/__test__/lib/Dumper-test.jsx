/* eslint-disable no-unused-expressions */
import Dumper from './../../lib/Dumper';

describe('Dumper', () => {
  it('Dumper dump successfully', () => {
    const val = Dumper.dump({
      hue: ['tes1', 'tes2'],
      hui: { huo: '123' },
    });
    expect(val).to.be.exist;
  });

  it('Dumper dump correctly', () => {
    const val = Dumper.dump({ hue: [1, 2, 3] });
    expect(val).to.equal('hue : [ 1,2,3 ]');
  })
});
