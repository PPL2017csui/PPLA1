import React from 'react';
import ReactTestUtils from 'react-addons-test-utils';
import fetchMock from 'fetch-mock';
import TranscriptPage from '../TranscriptPage';

describe('TranscriptPage', () => {
  const data = {
    transcript: [
      { kelas: { nm_kls: 'kelas1', nm_mk_cl: { nm_mk: 'mata_kuliah' } }, nilai: 'A' },
      { kelas: { nm_kls: 'kelas2', nm_mk_cl: { nm_mk: 'mata_kuliah' } }, nilai: 'B' },
      { nilai: 'B' },
    ],
    name: 'Badak Terbang',
  };

  fetchMock.get('*', data);

  it('renders for admin without problem', () => {
    const transcriptPage1 = ReactTestUtils.renderIntoDocument(<TranscriptPage params={{ id: 1 }} user={{ role: 'student' }} />);
    const transcriptPage2 = ReactTestUtils.renderIntoDocument(<TranscriptPage params={{ id: 1 }} user={{ role: 'admin' }} />);
    transcriptPage1.setState({ data });
    transcriptPage2.setState({ data });
    expect(transcriptPage1).to.exist;
    expect(transcriptPage2).to.exist;
  });
});
