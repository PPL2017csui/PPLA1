/* eslint-disable no-unused-expressions */
import React from 'react';
import ReactTestUtils from 'react-addons-test-utils';
import fetchMock from 'fetch-mock';
import ProfilePage from '../ProfilePage';

describe('ProfilePage', () => {
  const studentSession = {
    url: 'http://localhost:8000/api/users/9/',
    username: 'muhammad.reza42',
    email: 'muhammad.reza42@ui.ac.id',
    is_staff: false,
    company: null,
    supervisor: null,
    student: {
      id: 3,
      user: {
        url: 'http://localhost:8000/api/users/9/',
        username: 'muhammad.reza42',
        email: 'muhammad.reza42@ui.ac.id',
        is_staff: false,
      },
      name: 'Muhammad R.',
      created: '2017-03-28T13:33:46.147241Z',
      updated: '2017-03-28T13:33:46.148248Z',
      npm: 1406543593,
      resume: null,
      phone_number: null,
      birth_place: null,
      birth_date: null,
      major: null,
      batch: null,
      show_resume: false,
      bookmarked_vacancies: [
        3,
        2,
      ],
      applied_vacancies: [
        3,
        1,
      ],
    },
  };

  const companyUser = {
    role: 'company',
    data: {
      url: 'http://localhost:8001/api/users/8/',
      username: 'Tutuplapak',
      email: '',
      is_staff: false,
      company: {
        id: 3,
        user: {
          url: 'http://localhost:8001/api/users/8/',
          username: 'Tutuplapak',
          email: '',
          is_staff: false,
        },
        name: 'Tutuplapak',
        created: '2017-03-28T07:30:10.535000Z',
        updated: '2017-03-28T07:30:10.535000Z',
        description: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla aliquet semper neque a fermentum. Duis ac tellus vitae augue iaculis ultrices. Curabitur commodo et neque nec feugiat. Morbi ac diam vel nunc commodo cursus. Phasellus nulla sapien, hendrerit vitae bibendum at, sollicitudin eu ante. Maecenas maximus, ante eu sollicitudin convallis, mauris nunc posuere risus, eu porttitor diam lacus vitae enim. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Suspendisse at lectus a elit sollicitudin tempor. Nullam condimentum, justo nec tincidunt maximus, neque mi vulputate leo, sit amet lacinia massa ex eget sem. Duis ac erat facilisis, fringilla mauris in, consequat neque. In et neque consequat, vehicula magna at, efficitur ante. Mauris ac lacinia nibh.\r\n\r\nProin sagittis, lectus quis maximus varius, libero justo sollicitudin augue, non lacinia risus orci a enim. Curabitur iaculis enim quis ullamcorper commodo. Vivamus id nisi rhoncus, dignissim tellus quis, interdum est. Fusce sollicitudin eu libero ac feugiat. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Maecenas semper posuere ex, sed accumsan libero iaculis faucibus. Fusce laoreet ac ligula ut consectetur. Donec tortor mauris, rutrum at sodales et, viverra in dolor. Sed bibendum elit et maximus volutpat. Phasellus justo ipsum, laoreet sit amet faucibus eu, ultricies suscipit mauris. Nullam aliquam libero eu ante ultrices mattis. Donec non justo hendrerit neque volutpat placerat. Ut euismod est nec sem mollis, sit amet porttitor massa rhoncus. Aenean id erat sit amet nunc ultrices scelerisque non in ipsum. Curabitur sollicitudin nulla id mi accumsan venenatis.',
        verified: true,
        logo: 'http://localhost:8001/files/company-logo/8a258a48-3bce-4873-b5d1-538b360d0059.png',
        address: 'Jl. Kebayoran Baru nomor 13, Jakarta Barat',
      },
      supervisor: null,
      student: null,
    },
  };

  const response = {
    id: 3,
    name: 'Muhammad R.',
    user: {
      url: 'http://localhost:8000/api/users/9/',
      username: 'muhammad.reza42',
      email: 'muhammad.reza42@ui.ac.id',
      is_staff: false,
    },
    npm: 1406543593,
    resume: null,
    phone_number: null,
    birth_place: null,
    birth_date: null,
    photo: 'dor',
    major: null,
    batch: null,
    show_transcript: false,
  };

  const response2 = {
    id: 3,
    name: 'Muhammad R.',
    user: {
      url: 'http://localhost:8000/api/users/9/',
      username: 'muhammad.reza42',
      email: 'muhammad.reza42@ui.ac.id',
      is_staff: false,
    },
    npm: 1406543593,
    resume: null,
    phone_number: null,
    birth_place: null,
    birth_date: null,
    major: null,
    batch: null,
    show_transcript: true,
  };

  it('renders without problem', () => {
    fetchMock.get('*', response);
    const profile = ReactTestUtils.renderIntoDocument(
      <ProfilePage route={{ own: true, data: studentSession }} user={{ data: studentSession }} params={{}} />);
    profile.getProfile().then(()=> expect(profile.state.name).to.equal(response.name));
    profile.updateForm(true);
    fetchMock.restore();
  });

  it('get profile for company without problem', () => {
    fetchMock.get('*', response);
    const profile = ReactTestUtils.renderIntoDocument(
      <ProfilePage route={{ own: false, data: studentSession }} user={{ data: companyUser }} params={{ id: 3 }} />);
    profile.getProfile().then(()=> expect(profile.state.name).to.equal(response.name));
    fetchMock.restore();
  });

  it('get profile for company without problem', () => {
    fetchMock.get('*', response2);
    const profile = ReactTestUtils.renderIntoDocument(
      <ProfilePage route={{ own: false, data: studentSession }} user={{ data: companyUser }} params={{ id: 3 }} />);
    profile.getProfile().then(()=> expect(profile.state.name).to.equal(response2.name));
    fetchMock.restore();
  });

  it('show/hide transcript without problem', () => {
    fetchMock.get('*', response2);
    const profile = ReactTestUtils.renderIntoDocument(
      <ProfilePage route={{ own: true, data: studentSession }} user={{ data: studentSession }} params={{ id: 3 }} />);

    const checkboxNode = ReactTestUtils.scryRenderedDOMComponentsWithTag(profile, 'Input')[4];
    const checkbox = false;
    checkboxNode.value = checkbox;
    profile.getProfile().then(()=> expect(profile.state.show_transcript).to.equal(true));
    profile.handleCheckbox({name: 'show_transcript', value: checkbox}, {name: 'show_transcript', checked: false});
    // ReactTestUtils.Simulate.change(checkboxNode, { target: {name: 'show_transcript', value: checkbox} }, {name: 'show_transcript', checked: false});
    expect(profile.state.form.show_transcript).to.equal(false);
    fetchMock.restore();
  });

  it('renders without problem when error getting data', () => {
    fetchMock.get('*', 400);
    const profile = ReactTestUtils.renderIntoDocument(
      <ProfilePage route={{ own: false, data: studentSession }} user={{ data: studentSession }} params={{ id: 3 }} />);
    profile.getProfile().then(()=> expect(profile.state.name).to.equal('Gagal mendapatkan informasi'));
  });

  it('submit form without problem', () => {
    fetchMock.patch('*', { data: 'value' });
    const profile = ReactTestUtils.renderIntoDocument(
      <ProfilePage route={{ own: true, data: studentSession }} user={{ data: studentSession }} params={{ id: 3 }} />);

    profile.state.form = {name: 'abra', email:''};
    const submitButton = ReactTestUtils.scryRenderedDOMComponentsWithTag(profile, 'Input')[1];
    ReactTestUtils.Simulate.click(submitButton);

    const form = ReactTestUtils.findRenderedDOMComponentWithTag(profile, 'Form');
    ReactTestUtils.Simulate.submit(form);
    fetchMock.restore();
  });

  it('render without problem when submit file error', () => {
    fetchMock.patch('*', 400);
    fetchMock.get('*', response);
    const profile = ReactTestUtils.renderIntoDocument(
      <ProfilePage route={{ own: true, data: studentSession }} user={{ data: studentSession }} params={{ id: 3 }} />);

    profile.state.form = {name: 'abra', email:''};
    const submitButton = ReactTestUtils.scryRenderedDOMComponentsWithTag(profile, 'Input')[1];
    ReactTestUtils.Simulate.click(submitButton);

    const form = ReactTestUtils.findRenderedDOMComponentWithTag(profile, 'Form');
    ReactTestUtils.Simulate.submit(form);
    fetchMock.restore();
  });

  it('handle email input without problem', () => {
    fetchMock.patch('*', { data: 'value' });
    fetchMock.get('*', response);
    const profile = ReactTestUtils.renderIntoDocument(
      <ProfilePage route={{ own: true, data: studentSession }} user={{ data: studentSession }} params={{ id: 3 }} />);

    const emailNode = ReactTestUtils.scryRenderedDOMComponentsWithTag(profile, 'Input')[1];
    // const passwordNode = ReactDOM.findDOMNode(formLogin.refs.password);
    const password = 'passwd';
    emailNode.value = password;
    expect(profile.state.form.email).to.equal('');
    ReactTestUtils.Simulate.change(emailNode, { target: { value: password } });
    expect(emailNode.value).to.equal(password);
  });

  it('handle file input without problem', () => {
    fetchMock.patch('*', { data: 'value' });
    fetchMock.get('*', response);
    const profile = ReactTestUtils.renderIntoDocument(
      <ProfilePage route={{ own: true, data: studentSession }} user={{ data: studentSession }} params={{ id: 3 }} />);

    const emailNode = ReactTestUtils.scryRenderedDOMComponentsWithTag(profile, 'Input')[0];
    // const passwordNode = ReactDOM.findDOMNode(formLogin.refs.password);
    profile.handleFile({ target: { name: 'photo', files: ['abc', 'def'] } });
    expect(profile.state.form.photo).to.equal('abc');
  });


});
