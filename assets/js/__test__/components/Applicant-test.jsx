import React from 'react';
import ReactTestUtils from 'react-addons-test-utils';
import Applicant from '../../components/Applicant';
import fetchMock from 'fetch-mock';

describe('Applicant', () => {
  const stub = {
    data: {
      vacancy: { name: 'name' },
      student: { major: 'hue' },
      status: 2,
    },
    updateStatus: () => {},
  };

  const stub2 = {
    data: {
      vacancy: { name: 'name' },
      student: { major: 'hue' },
      status: 3,
    },
    updateStatus: () => {},
  };

  it('renders without problem', () => {
    fetchMock.get('*', stub);
    const applicant = ReactTestUtils.renderIntoDocument(
      <Applicant data={stub.data} updateStatus={stub.updateStatus} />,
    );
    expect(applicant).to.exist;
    fetchMock.restore();
  });

  it('bookmarks without problem', () => {
    fetchMock.get('*', stub);
    fetchMock.patch('*', {});
    const applicant = ReactTestUtils.renderIntoDocument(
      <Applicant data={stub2.data} updateStatus={stub.updateStatus} />,
    );
    applicant.bookmark();
    expect(applicant).to.exist;
    fetchMock.restore();
  });

  it('bookmarks with problem', () => {
    fetchMock.get('*', stub);
    fetchMock.patch('*', {});
    const applicant = ReactTestUtils.renderIntoDocument(
      <Applicant data={stub.data} updateStatus={stub.updateStatus} />,
    );
    applicant.bookmark();
    expect(applicant).to.exist;
    fetchMock.restore();
  });
});
