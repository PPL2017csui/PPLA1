/* eslint-disable no-unused-expressions */
import React from 'react';
import ReactTestUtils from 'react-addons-test-utils';
import fetchMock from 'fetch-mock';
import VacancyList from '../../components/VacancyList';

describe('VacancyList', () => {

  const companyUser = {
    role: 'company',
    data: {
      url: 'http://localhost:8001/api/users/8/',
      username: 'Tutuplapak',
      email: '',
      is_staff: false,
      company: {
        id: 3,
        user: {
          url: 'http://localhost:8001/api/users/8/',
          username: 'Tutuplapak',
          email: '',
          is_staff: false,
        },
        name: 'Tutuplapak',
        created: '2017-03-28T07:30:10.535000Z',
        updated: '2017-03-28T07:30:10.535000Z',
        description: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla aliquet semper neque a fermentum. Duis ac tellus vitae augue iaculis ultrices. Curabitur commodo et neque nec feugiat. Morbi ac diam vel nunc commodo cursus. Phasellus nulla sapien, hendrerit vitae bibendum at, sollicitudin eu ante. Maecenas maximus, ante eu sollicitudin convallis, mauris nunc posuere risus, eu porttitor diam lacus vitae enim. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Suspendisse at lectus a elit sollicitudin tempor. Nullam condimentum, justo nec tincidunt maximus, neque mi vulputate leo, sit amet lacinia massa ex eget sem. Duis ac erat facilisis, fringilla mauris in, consequat neque. In et neque consequat, vehicula magna at, efficitur ante. Mauris ac lacinia nibh.\r\n\r\nProin sagittis, lectus quis maximus varius, libero justo sollicitudin augue, non lacinia risus orci a enim. Curabitur iaculis enim quis ullamcorper commodo. Vivamus id nisi rhoncus, dignissim tellus quis, interdum est. Fusce sollicitudin eu libero ac feugiat. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Maecenas semper posuere ex, sed accumsan libero iaculis faucibus. Fusce laoreet ac ligula ut consectetur. Donec tortor mauris, rutrum at sodales et, viverra in dolor. Sed bibendum elit et maximus volutpat. Phasellus justo ipsum, laoreet sit amet faucibus eu, ultricies suscipit mauris. Nullam aliquam libero eu ante ultrices mattis. Donec non justo hendrerit neque volutpat placerat. Ut euismod est nec sem mollis, sit amet porttitor massa rhoncus. Aenean id erat sit amet nunc ultrices scelerisque non in ipsum. Curabitur sollicitudin nulla id mi accumsan venenatis.',
        verified: true,
        logo: 'http://localhost:8001/files/company-logo/8a258a48-3bce-4873-b5d1-538b360d0059.png',
        address: 'Jl. Kebayoran Baru nomor 13, Jakarta Barat',
      },
      supervisor: null,
      student: null,
    },
  };

  const supervisorUser = {
    role: 'supervisor',
    data: {
      url: 'http://localhost:8001/api/users/8/',
      username: 'Tutuplapak',
      email: '',
      is_staff: false,
      company: null,
      supervisor: {
        id: 3,
        user: {
          url: 'http://localhost:8000/api/users/9/',
          username: 'muhammad.reza42',
          email: 'muhammad.reza42@ui.ac.id',
          is_staff: false,
        },
        name: 'Muhammad R.',
        created: '2017-03-28T13:33:46.147241Z',
        updated: '2017-03-28T13:33:46.148248Z',
        npm: 1406543593,
        resume: null,
        phone_number: null,
        birth_place: null,
        birth_date: null,
        major: null,
        batch: null,
        show_resume: false,
        bookmarked_vacancies: [
          3,
          2,
        ],
        applied_vacancies: [
          3,
          1,
        ],
      },
      student: null,
    },
  };

  const studentUser = {
    role: 'student',
    data: {
      url: 'http://localhost:8001/api/users/8/',
      username: 'Tutuplapak',
      email: '',
      is_staff: false,
      student: {
        id: 3,
        user: {
          url: 'http://localhost:8000/api/users/9/',
          username: 'muhammad.reza42',
          email: 'muhammad.reza42@ui.ac.id',
          is_staff: false,
        },
        name: 'Muhammad R.',
        created: '2017-03-28T13:33:46.147241Z',
        updated: '2017-03-28T13:33:46.148248Z',
        npm: 1406543593,
        resume: null,
        phone_number: null,
        birth_place: null,
        birth_date: null,
        major: null,
        batch: null,
        show_resume: false,
        bookmarked_vacancies: [
          3,
          2,
        ],
        applied_vacancies: [
          3,
          1,
        ],
      },
      company: null,
      supervisor: null,
    },
  };

  const newResponse = [
    {
      company: {
        id: 3,
        user: {
          url: 'http://localhost:8000/api/users/8/',
          username: 'Tutuplapak',
          email: '',
          is_staff: false,
        },
        name: 'Tutuplapak',
        created: '2017-03-28T07:30:10.535240Z',
        updated: '2017-05-07T13:22:19.175033Z',
        description: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla aliquet semper neque a fermentum. Duis ac tellus vitae augue iaculis ultrices. Curabitur commodo et neque nec feugiat. Morbi ac diam vel nunc commodo cursus. Phasellus nulla sapien, hendrerit vitae bibendum at, sollicitudin eu ante. Maecenas maximus, ante eu sollicitudin convallis, mauris nunc posuere risus, eu porttitor diam lacus vitae enim. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Suspendisse at lectus a elit sollicitudin tempor. Nullam condimentum, justo nec tincidunt maximus, neque mi vulputate leo, sit amet lacinia massa ex eget sem. Duis ac erat facilisis, fringilla mauris in, consequat neque. In et neque consequat, vehicula magna at, efficitur ante. Mauris ac lacinia nibh.\r\n\r\nProin sagittis, lectus quis maximus varius, libero justo sollicitudin augue, non lacinia risus orci a enim. Curabitur iaculis enim quis ullamcorper commodo. Vivamus id nisi rhoncus, dignissim tellus quis, interdum est. Fusce sollicitudin eu libero ac feugiat. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Maecenas semper posuere ex, sed accumsan libero iaculis faucibus. Fusce laoreet ac ligula ut consectetur. Donec tortor mauris, rutrum at sodales et, viverra in dolor. Sed bibendum elit et maximus volutpat. Phasellus justo ipsum, laoreet sit amet faucibus eu, ultricies suscipit mauris. Nullam aliquam libero eu ante ultrices mattis. Donec non justo hendrerit neque volutpat placerat. Ut euismod est nec sem mollis, sit amet porttitor massa rhoncus. Aenean id erat sit amet nunc ultrices scelerisque non in ipsum. Curabitur sollicitudin nulla id mi accumsan venenatis.',
        registeredStatus: 1,
        logo: null,
        address: 'Jl. Kebayoran Baru nomor 13, Jakarta Barat',
      },
      verified: true,
      open_time: '2017-04-26T03:39:11Z',
      description: 'deskripsi',
      close_time: '2017-04-30T03:39:11Z',
      created: '2017-04-26T03:39:39.916758Z',
      updated: '2017-04-26T03:41:07.157634Z',
      name: 'Kepala Sekolah',
      status: 0,
      bookmarked: false,
      id: 4,
    },
    {
      company: {
        id: 3,
        user: {
          url: 'http://localhost:8000/api/users/8/',
          username: 'Tutuplapak',
          email: '',
          is_staff: false,
        },
        name: 'Tutuplapak',
        created: '2017-03-28T07:30:10.535240Z',
        updated: '2017-05-07T13:22:19.175033Z',
        description: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla aliquet semper neque a fermentum. Duis ac tellus vitae augue iaculis ultrices. Curabitur commodo et neque nec feugiat. Morbi ac diam vel nunc commodo cursus. Phasellus nulla sapien, hendrerit vitae bibendum at, sollicitudin eu ante. Maecenas maximus, ante eu sollicitudin convallis, mauris nunc posuere risus, eu porttitor diam lacus vitae enim. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Suspendisse at lectus a elit sollicitudin tempor. Nullam condimentum, justo nec tincidunt maximus, neque mi vulputate leo, sit amet lacinia massa ex eget sem. Duis ac erat facilisis, fringilla mauris in, consequat neque. In et neque consequat, vehicula magna at, efficitur ante. Mauris ac lacinia nibh.\r\n\r\nProin sagittis, lectus quis maximus varius, libero justo sollicitudin augue, non lacinia risus orci a enim. Curabitur iaculis enim quis ullamcorper commodo. Vivamus id nisi rhoncus, dignissim tellus quis, interdum est. Fusce sollicitudin eu libero ac feugiat. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Maecenas semper posuere ex, sed accumsan libero iaculis faucibus. Fusce laoreet ac ligula ut consectetur. Donec tortor mauris, rutrum at sodales et, viverra in dolor. Sed bibendum elit et maximus volutpat. Phasellus justo ipsum, laoreet sit amet faucibus eu, ultricies suscipit mauris. Nullam aliquam libero eu ante ultrices mattis. Donec non justo hendrerit neque volutpat placerat. Ut euismod est nec sem mollis, sit amet porttitor massa rhoncus. Aenean id erat sit amet nunc ultrices scelerisque non in ipsum. Curabitur sollicitudin nulla id mi accumsan venenatis.',
        registeredStatus: 1,
        logo: 'http://localhost:8000/files/company-logo/8a258a48-3bce-4873-b5d1-538b360d0059.png',
        address: 'Jl. Kebayoran Baru nomor 13, Jakarta Barat',
      },
      verified: true,
      open_time: '2017-04-26T03:39:11Z',
      description: 'deskripsi',
      close_time: '2017-04-30T03:39:11Z',
      created: '2017-04-26T03:39:39.916758Z',
      updated: '2017-04-26T03:41:07.157634Z',
      name: 'Kepala Sekolah',
      status: 1,
      bookmarked: false,
      id: 4,
    },
    {
      company: {
        id: 3,
        user: {
          url: 'http://localhost:8000/api/users/8/',
          username: 'Tutuplapak',
          email: '',
          is_staff: false,
        },
        name: 'Tutuplapak',
        created: '2017-03-28T07:30:10.535240Z',
        updated: '2017-05-07T13:22:19.175033Z',
        description: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla aliquet semper neque a fermentum. Duis ac tellus vitae augue iaculis ultrices. Curabitur commodo et neque nec feugiat. Morbi ac diam vel nunc commodo cursus. Phasellus nulla sapien, hendrerit vitae bibendum at, sollicitudin eu ante. Maecenas maximus, ante eu sollicitudin convallis, mauris nunc posuere risus, eu porttitor diam lacus vitae enim. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Suspendisse at lectus a elit sollicitudin tempor. Nullam condimentum, justo nec tincidunt maximus, neque mi vulputate leo, sit amet lacinia massa ex eget sem. Duis ac erat facilisis, fringilla mauris in, consequat neque. In et neque consequat, vehicula magna at, efficitur ante. Mauris ac lacinia nibh.\r\n\r\nProin sagittis, lectus quis maximus varius, libero justo sollicitudin augue, non lacinia risus orci a enim. Curabitur iaculis enim quis ullamcorper commodo. Vivamus id nisi rhoncus, dignissim tellus quis, interdum est. Fusce sollicitudin eu libero ac feugiat. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Maecenas semper posuere ex, sed accumsan libero iaculis faucibus. Fusce laoreet ac ligula ut consectetur. Donec tortor mauris, rutrum at sodales et, viverra in dolor. Sed bibendum elit et maximus volutpat. Phasellus justo ipsum, laoreet sit amet faucibus eu, ultricies suscipit mauris. Nullam aliquam libero eu ante ultrices mattis. Donec non justo hendrerit neque volutpat placerat. Ut euismod est nec sem mollis, sit amet porttitor massa rhoncus. Aenean id erat sit amet nunc ultrices scelerisque non in ipsum. Curabitur sollicitudin nulla id mi accumsan venenatis.',
        registeredStatus: 1,
        logo: null,
        address: 'Jl. Kebayoran Baru nomor 13, Jakarta Barat',
      },
      verified: true,
      open_time: '2017-04-26T03:39:11Z',
      description: 'deskripsi',
      close_time: '2017-04-30T03:39:11Z',
      created: '2017-04-26T03:39:39.916758Z',
      updated: '2017-04-26T03:41:07.157634Z',
      name: 'Kepala Sekolah',
      status: 2,
      bookmarked: true,
      id: 4,
    },
    {
      company: {
        id: 3,
        user: {
          url: 'http://localhost:8000/api/users/8/',
          username: 'Tutuplapak',
          email: '',
          is_staff: false,
        },
        name: 'Tutuplapak',
        created: '2017-03-28T07:30:10.535240Z',
        updated: '2017-05-07T13:22:19.175033Z',
        description: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla aliquet semper neque a fermentum. Duis ac tellus vitae augue iaculis ultrices. Curabitur commodo et neque nec feugiat. Morbi ac diam vel nunc commodo cursus. Phasellus nulla sapien, hendrerit vitae bibendum at, sollicitudin eu ante. Maecenas maximus, ante eu sollicitudin convallis, mauris nunc posuere risus, eu porttitor diam lacus vitae enim. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Suspendisse at lectus a elit sollicitudin tempor. Nullam condimentum, justo nec tincidunt maximus, neque mi vulputate leo, sit amet lacinia massa ex eget sem. Duis ac erat facilisis, fringilla mauris in, consequat neque. In et neque consequat, vehicula magna at, efficitur ante. Mauris ac lacinia nibh.\r\n\r\nProin sagittis, lectus quis maximus varius, libero justo sollicitudin augue, non lacinia risus orci a enim. Curabitur iaculis enim quis ullamcorper commodo. Vivamus id nisi rhoncus, dignissim tellus quis, interdum est. Fusce sollicitudin eu libero ac feugiat. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Maecenas semper posuere ex, sed accumsan libero iaculis faucibus. Fusce laoreet ac ligula ut consectetur. Donec tortor mauris, rutrum at sodales et, viverra in dolor. Sed bibendum elit et maximus volutpat. Phasellus justo ipsum, laoreet sit amet faucibus eu, ultricies suscipit mauris. Nullam aliquam libero eu ante ultrices mattis. Donec non justo hendrerit neque volutpat placerat. Ut euismod est nec sem mollis, sit amet porttitor massa rhoncus. Aenean id erat sit amet nunc ultrices scelerisque non in ipsum. Curabitur sollicitudin nulla id mi accumsan venenatis.',
        registeredStatus: 1,
        logo: null,
        address: 'Jl. Kebayoran Baru nomor 13, Jakarta Barat',
      },
      verified: true,
      open_time: '2017-04-26T03:39:11Z',
      description: 'deskripsi',
      close_time: '2017-04-30T03:39:11Z',
      created: '2017-04-26T03:39:39.916758Z',
      updated: '2017-04-26T03:41:07.157634Z',
      name: 'Kepala Sekolah',
      status: 3,
      bookmarked: true,
      id: 4,
    },
    {
      company: {
        id: 3,
        user: {
          url: 'http://localhost:8000/api/users/8/',
          username: 'Tutuplapak',
          email: '',
          is_staff: false,
        },
        name: 'Tutuplapak',
        created: '2017-03-28T07:30:10.535240Z',
        updated: '2017-05-07T13:22:19.175033Z',
        description: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla aliquet semper neque a fermentum. Duis ac tellus vitae augue iaculis ultrices. Curabitur commodo et neque nec feugiat. Morbi ac diam vel nunc commodo cursus. Phasellus nulla sapien, hendrerit vitae bibendum at, sollicitudin eu ante. Maecenas maximus, ante eu sollicitudin convallis, mauris nunc posuere risus, eu porttitor diam lacus vitae enim. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Suspendisse at lectus a elit sollicitudin tempor. Nullam condimentum, justo nec tincidunt maximus, neque mi vulputate leo, sit amet lacinia massa ex eget sem. Duis ac erat facilisis, fringilla mauris in, consequat neque. In et neque consequat, vehicula magna at, efficitur ante. Mauris ac lacinia nibh.\r\n\r\nProin sagittis, lectus quis maximus varius, libero justo sollicitudin augue, non lacinia risus orci a enim. Curabitur iaculis enim quis ullamcorper commodo. Vivamus id nisi rhoncus, dignissim tellus quis, interdum est. Fusce sollicitudin eu libero ac feugiat. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Maecenas semper posuere ex, sed accumsan libero iaculis faucibus. Fusce laoreet ac ligula ut consectetur. Donec tortor mauris, rutrum at sodales et, viverra in dolor. Sed bibendum elit et maximus volutpat. Phasellus justo ipsum, laoreet sit amet faucibus eu, ultricies suscipit mauris. Nullam aliquam libero eu ante ultrices mattis. Donec non justo hendrerit neque volutpat placerat. Ut euismod est nec sem mollis, sit amet porttitor massa rhoncus. Aenean id erat sit amet nunc ultrices scelerisque non in ipsum. Curabitur sollicitudin nulla id mi accumsan venenatis.',
        registeredStatus: 1,
        logo: null,
        address: 'Jl. Kebayoran Baru nomor 13, Jakarta Barat',
      },
      verified: true,
      open_time: '2017-04-26T03:39:11Z',
      description: 'deskripsi',
      close_time: '2017-04-30T03:39:11Z',
      created: '2017-04-26T03:39:39.916758Z',
      updated: '2017-04-26T03:41:07.157634Z',
      name: 'Kepala Sekolah',
      status: 4,
      bookmarked: true,
      id: 4,
    },
  ];

  const response = [{
    close_time: '2019-03-28T05:55:42Z',
    company: {
      address: 'kebayoran baru',
      id: 1,
      logo: null,
      name: 'tutup lapak',
    },
    created: '2017-03-28T07:05:47.128672Z',
    description: 'Lorem ipsum dolbh.',
    id: 1,
    name: 'Software Engineer',
    open_time: '2017-03-28T05:55:38Z',
    updated: '2017-03-28T07:34:13.122093Z',
    verified: true,
  }, {
    close_time: '2019-03-28T05:55:42Z',
    company: {
      address: 'kebayoran baru',
      id: 2,
      logo: null,
      name: 'tutup lapak',
    },
    created: '2017-03-28T07:05:47.128672Z',
    description: 'Lorem ipsum dolbh.',
    id: 2,
    name: 'Software Engineer',
    open_time: '2017-03-28T05:55:38Z',
    updated: '2017-03-28T07:34:13.122093Z',
    verified: true,
  }];

  const response2 = [{
    close_time: '2019-03-28T05:55:42Z',
    company: {
      address: 'kebayoran baru',
      id: 1,
      logo: null,
      name: 'tutup lapak',
    },
    created: '2017-03-28T07:05:47.128672Z',
    description: 'Lorem ipsum dolbh.',
    id: 2,
    name: 'Software Engineer',
    open_time: '2017-03-28T05:55:38Z',
    updated: '2017-03-28T07:34:13.122093Z',
    verified: true,
  }];

  it('renders without problem', () => {
    const vacancyList = ReactTestUtils.renderIntoDocument(
      <VacancyList items={newResponse} userId={1} user={studentUser} />);
    vacancyList.generateVacancies();
    expect(vacancyList).to.exist;
  });

  it('renders without problem for company', () => {
    const vacancyList = ReactTestUtils.renderIntoDocument(
      <VacancyList items={newResponse} userId={1} user={companyUser} />);
    vacancyList.generateVacancies();
    vacancyList.state.vacancies = newResponse;
    expect(vacancyList.generateVacancies()).to.exist;
  });

  it('renders without problem for supervisor', () => {
    const vacancyList = ReactTestUtils.renderIntoDocument(
      <VacancyList items={newResponse} userId={3} user={supervisorUser} />);
    vacancyList.state.vacancies = newResponse;
    expect(vacancyList.generateVacancies()).to.exist;
  });

  it('update status without problem', () => {
    const vacancyList = ReactTestUtils.renderIntoDocument(
      <VacancyList items={newResponse} userId={3} user={supervisorUser} />);
    vacancyList.state.vacancies = newResponse;
    vacancyList.updateStatus(4, 1);
  });

  it('success delete vacancy', () => {
    fetchMock.restore();
    fetchMock.delete('*', response2);
    fetchMock.get('*', newResponse);
    const vacancyList = ReactTestUtils.renderIntoDocument(
      <VacancyList items={newResponse} userId={1} deleteCallback={() => {}} user={companyUser} />);
    vacancyList.state.vacancies = newResponse;
    vacancyList.deleteVacancy(1).then(() => {
      expect(JSON.stringify(vacancyList.state.vacancies)).to.equal(JSON.stringify(newResponse));
      fetchMock.restore();
    }, () => {
      fetchMock.restore();
    });
  });

  it('fails delete vacancy', (done) => {
    fetchMock.restore();
    fetchMock.delete('*', 404);
    fetchMock.get('*', response2);
    const vacancyList = ReactTestUtils.renderIntoDocument(
      <VacancyList userId={1} items={newResponse} user={companyUser} deleteCallback={() => {}} />,
    );
    vacancyList.state.vacancies = response;
    vacancyList.deleteVacancy(1).then(() => {
      fetchMock.restore();
      done();
    }, () => {
      expect(JSON.stringify(vacancyList.state.vacancies)).to.equal(JSON.stringify(response));
      fetchMock.restore();
      done();
    });
  });
});

