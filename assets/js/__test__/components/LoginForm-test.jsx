/* eslint-disable no-unused-expressions */
import React from 'react';
import ReactTestUtils from 'react-addons-test-utils';
import LoginForm from '../../components/LoginForm';

describe('LoginForm', () => {
  const fetchMock = require('fetch-mock');
  it('created without problem', () => {
    const formLogin = new LoginForm({ url: 'tes' });
    expect(formLogin).to.be.an.instanceof(LoginForm);
  });

  it('renders without problem', () => {
    const formLogin = ReactTestUtils.renderIntoDocument(
      <LoginForm url="" />);
    expect(formLogin).to.exist;

    const formDiv = ReactTestUtils.findRenderedDOMComponentWithClass(
      formLogin, 'formLogin');
    expect(formDiv).to.exist;

    const form = ReactTestUtils.findRenderedDOMComponentWithTag(
      formLogin, 'form');
    expect(form).to.exist;
  });

  it('handle email input without problem', () => {
    const formLogin = ReactTestUtils.renderIntoDocument(
      <LoginForm type="sso-ui" />);
    expect(formLogin.state.username).to.equal('');
    const emailNode = ReactTestUtils.scryRenderedDOMComponentsWithTag(formLogin, 'Input')[0];
    // const emailNode = ReactDOM.findDOMNode(formLogin.refs.email);
    const email = 'jojon';
    emailNode.value = email;

    ReactTestUtils.Simulate.change(emailNode, { target: { value: email } });
    expect(emailNode.value).to.equal(email);
    expect(formLogin.state.username).to.equal(email);
  });

  it('handle password input without problem', () => {
    const formLogin = ReactTestUtils.renderIntoDocument(
      <LoginForm type="sso-ui" />);

    const passwordNode = ReactTestUtils.scryRenderedDOMComponentsWithTag(formLogin, 'Input')[1];
    // const passwordNode = ReactDOM.findDOMNode(formLogin.refs.password);
    const password = 'passwd';
    passwordNode.value = password;
    expect(formLogin.state.password).to.equal('');
    ReactTestUtils.Simulate.change(passwordNode, { target: { value: password } });
    expect(passwordNode.value).to.equal(password);
    expect(formLogin.state.password).to.equal(password);
  });

  it('submit form without problem', () => {
    fetchMock.post('*', { data: 'value' });
    const formLogin = ReactTestUtils.renderIntoDocument(
      <LoginForm url="" />);

    const submitButton = ReactTestUtils.findRenderedDOMComponentWithTag(formLogin, 'Button');
    ReactTestUtils.Simulate.click(submitButton);

    const form = ReactTestUtils.findRenderedDOMComponentWithTag(formLogin, 'Form');
    ReactTestUtils.Simulate.submit(form);
    fetchMock.restore();
  });

  it('submit form with problem', () => {
    fetchMock.post('*', 404);
    const formLogin = ReactTestUtils.renderIntoDocument(
      <LoginForm url="" />);

    const submitButton = ReactTestUtils.findRenderedDOMComponentWithTag(formLogin, 'Button');
    ReactTestUtils.Simulate.click(submitButton);

    const form = ReactTestUtils.findRenderedDOMComponentWithTag(formLogin, 'Form');
    ReactTestUtils.Simulate.submit(form);
    fetchMock.restore();
  });
});
