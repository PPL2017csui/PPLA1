import React from 'react';
import ReactTestUtils from 'react-addons-test-utils';
import Company from '../../components/Company';
import fetchMock from 'fetch-mock';

describe('Company', () => {
  const stub = {
    id: 3,
    user: {
      url: 'http://localhost:8001/api/users/8/',
      username: 'Tutuplapak',
      email: '',
      is_staff: false,
    },
    name: 'Tutuplapak',
    created: '2017-03-28T07:30:10.535000Z',
    updated: '2017-03-28T07:30:10.535000Z',
    description: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla aliquet semper neque a fermentum. Duis ac tellus vitae augue iaculis ultrices. Curabitur commodo et neque nec feugiat. Morbi ac diam vel nunc commodo cursus. Phasellus nulla sapien, hendrerit vitae bibendum at, sollicitudin eu ante. Maecenas maximus, ante eu sollicitudin convallis, mauris nunc posuere risus, eu porttitor diam lacus vitae enim. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Suspendisse at lectus a elit sollicitudin tempor. Nullam condimentum, justo nec tincidunt maximus, neque mi vulputate leo, sit amet lacinia massa ex eget sem. Duis ac erat facilisis, fringilla mauris in, consequat neque. In et neque consequat, vehicula magna at, efficitur ante. Mauris ac lacinia nibh.\r\n\r\nProin sagittis, lectus quis maximus varius, libero justo sollicitudin augue, non lacinia risus orci a enim. Curabitur iaculis enim quis ullamcorper commodo. Vivamus id nisi rhoncus, dignissim tellus quis, interdum est. Fusce sollicitudin eu libero ac feugiat. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Maecenas semper posuere ex, sed accumsan libero iaculis faucibus. Fusce laoreet ac ligula ut consectetur. Donec tortor mauris, rutrum at sodales et, viverra in dolor. Sed bibendum elit et maximus volutpat. Phasellus justo ipsum, laoreet sit amet faucibus eu, ultricies suscipit mauris. Nullam aliquam libero eu ante ultrices mattis. Donec non justo hendrerit neque volutpat placerat. Ut euismod est nec sem mollis, sit amet porttitor massa rhoncus. Aenean id erat sit amet nunc ultrices scelerisque non in ipsum. Curabitur sollicitudin nulla id mi accumsan venenatis.',
    logo: 'http://localhost:8001/files/company-logo/8a258a48-3bce-4873-b5d1-538b360d0059.png',
    address: 'Jl. Kebayoran Baru nomor 13, Jakarta Barat',
  };

  const company1 = {};
  const company2 = {};
  Object.assign(company1, stub);
  Object.assign(company2, stub);
  company1.status = Company.COMPANY_STATUS.NEW;
  company2.status = Company.COMPANY_STATUS.VERIFIED;
  company2.logo = null;

  it('renders without problem', () => {
    fetchMock.get('*', company1);
    const company = ReactTestUtils.renderIntoDocument(
      <Company data={company1} updateStatus={() => {}} />,
    );
    expect(company).to.exist;
    fetchMock.restore();
  });

  it('accept without problem', () => {
    fetchMock.get('*', company1);
    fetchMock.patch('*', {});
    const company = ReactTestUtils.renderIntoDocument(
      <Company data={company2} updateStatus={() => {}} />,
    );
    company.accept();
    expect(company).to.exist;
    fetchMock.restore();
  });

  it('accept with problem', () => {
    fetchMock.get('*', company1);
    fetchMock.patch('*', 404);
    const company = ReactTestUtils.renderIntoDocument(
      <Company data={company1} updateStatus={() => {}} />,
    );
    company.accept();
    expect(company).to.exist;
    fetchMock.restore();
  });

  it('reject without problem', () => {
    fetchMock.get('*', company1);
    fetchMock.patch('*', {});
    const company = ReactTestUtils.renderIntoDocument(
      <Company data={company2} updateStatus={() => {}} />,
    );
    company.reject();
    expect(company).to.exist;
    fetchMock.restore();
  });

  it('reject with problem', () => {
    fetchMock.get('*', company1);
    fetchMock.patch('*', 404);
    const company = ReactTestUtils.renderIntoDocument(
      <Company data={company1} updateStatus={() => {}} />,
    );
    company.reject();
    expect(company).to.exist;
    fetchMock.restore();
  });

  it('go to company home click with problem', () => {
    fetchMock.get('*', company1);
    fetchMock.patch('*', 404);
    const company = ReactTestUtils.renderIntoDocument(
      <Company data={company1} updateStatus={() => {}} />,
    );
    company.goToCompanyHome();
    fetchMock.restore();
  });
});
