import React from 'react';
import ReactTestUtils from 'react-addons-test-utils';
import ConfirmationModal from '../../components/ConfirmationModal';
import fetchMock from 'fetch-mock';
import Storage from '../../lib/Storage';

describe('ConfirmationModal', () => {

  it('renders without problem', () => {
    const applyModal = ReactTestUtils.renderIntoDocument(
      <ConfirmationModal />,
    );
    expect(applyModal).to.exist;
  });

  it('test apply without problem', () => {
    const applyModal = ReactTestUtils.renderIntoDocument(
      <ConfirmationModal />,
    );
    const response = { student: { id: 1, name: 2 } };

    Storage.set('user-data', response);
    fetchMock.post('*', { data: 'value' });
    applyModal.open('Menghubungkan ke Server');
    expect(applyModal.state.header).to.equal('Menghubungkan ke Server');
    fetchMock.restore();
  });

  it('test apply with problem', () => {
    const applyModal = ReactTestUtils.renderIntoDocument(
      <ConfirmationModal />,
    );
    const response = { student: { id: 1, name: 2 } };

    Storage.set('user-data', response);
    fetchMock.post('*', 404);
    applyModal.open('Menghubungkan ke Server', '', '', () => {});
    applyModal.handleYes();
    expect(applyModal.state.header).to.equal('Menghubungkan ke Server');
    fetchMock.restore();
  });
});
