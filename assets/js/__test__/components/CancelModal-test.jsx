// /* eslint-disable no-unused-expressions */
// import React from 'react';
// import ReactTestUtils from 'react-addons-test-utils';
// import fetchMock from 'fetch-mock';
// import ConfirmationModal from '../../components/ConfirmationModal';
// import Storage from '../../lib/Storage';
//
// describe('ConfirmationModal', () => {
//   it('renders without problem', () => {
//     const modalPendaftaran = ReactTestUtils.renderIntoDocument(
//       <ConfirmationModal id={4} />);
//     expect(modalPendaftaran).to.exist;
//   });
//
//   it('open without problem', () => {
//     const modalPendaftaran = ReactTestUtils.renderIntoDocument(
//       <ConfirmationModal id={4} />);
//
//     const modal = ReactTestUtils.findRenderedDOMComponentWithTag(modalPendaftaran, 'Button');
//     ReactTestUtils.Simulate.click(modal);
//     expect(modalPendaftaran.state.modalOpen).to.equal(true);
//   });
//
//   it('close without problem', () => {
//     const modalPendaftaran = ReactTestUtils.renderIntoDocument(
//       <ConfirmationModal id={4} />);
//
//     modalPendaftaran.handleClose();
//     expect(modalPendaftaran.state.modalOpen).to.equal(false);
//   });
//
//   it('remove vacancy without problem', () => {
//     fetchMock.delete('*', { data: 'value' });
//     const modalPendaftaran = ReactTestUtils.renderIntoDocument(
//       <ConfirmationModal id={4} />);
//
//     const response3 = { student: { id: 1, name: 2 } };
//     Storage.set('user-data', response3);
//     modalPendaftaran.removeVacancy();
//     expect(modalPendaftaran.state.header).to.exist;
//     fetchMock.restore();
//   });
//
//   it('remove vacancy with problem', () => {
//     fetchMock.delete('*', 404);
//     const modalPendaftaran = ReactTestUtils.renderIntoDocument(
//       <ConfirmationModal id={4} />);
//
//     const response3 = { student: { id: 1, name: 2 } };
//     Storage.set('user-data', response3);
//     modalPendaftaran.removeVacancy();
//     expect(modalPendaftaran.state.header).to.exist;
//     fetchMock.restore();
//   });
//
//   it('confirm with problem', () => {
//     fetchMock.delete('*', 404);
//     const modalPendaftaran = ReactTestUtils.renderIntoDocument(
//       <ConfirmationModal id={4} />);
//
//     modalPendaftaran.confirm();
//     expect(modalPendaftaran.state.header).to.equal('Permintaan gagal');
//     fetchMock.restore();
//   });
//
//   it('render next modal without problem', () => {
//     const modalPendaftaran = ReactTestUtils.renderIntoDocument(
//       <ConfirmationModal id={4} />);
//
//     modalPendaftaran.state.confirmed = true;
//     modalPendaftaran.forceUpdate()
//     expect(modalPendaftaran).to.exist;
//   });
// });
