/* eslint-disable no-unused-expressions */
import React from 'react';
import ReactTestUtils from 'react-addons-test-utils';
import ApplyModal from '../../components/ApplyModal';
import fetchMock from 'fetch-mock';

describe('ApplyModal', () => {
  it('renders without problem', () => {
    const modalPendaftaran = ReactTestUtils.renderIntoDocument(
      <ApplyModal id={4} data={{ key: 'value' }} buttonTitle="submit" />);
    expect(modalPendaftaran).to.exist;
  });

  it('open without problem', () => {
    fetchMock.post('*', {});
    const modalPendaftaran = ReactTestUtils.renderIntoDocument(
      <ApplyModal id={4} data={{ key: 'value' }} buttonTitle="submit" apply={() => {}}/>);
    const modal = ReactTestUtils.findRenderedDOMComponentWithTag(modalPendaftaran, 'Button');
    modalPendaftaran.handleApply();
    ReactTestUtils.Simulate.click(modal);
    expect(modalPendaftaran).to.exist;
    fetchMock.restore();
  });

  it('open with problem', () => {
    fetchMock.post('*', 404);
    const modalPendaftaran = ReactTestUtils.renderIntoDocument(
      <ApplyModal id={4} data={{ key: 'value' }} buttonTitle="submit" apply={() => {}}/>);
    const modal = ReactTestUtils.findRenderedDOMComponentWithTag(modalPendaftaran, 'Button');
    modalPendaftaran.handleApply();
    ReactTestUtils.Simulate.click(modal);
    expect(modalPendaftaran).to.exist;
    fetchMock.restore();
  });

  it('change without problem', () => {
    const modalPendaftaran = ReactTestUtils.renderIntoDocument(
      <ApplyModal id={4} data={{ key: 'value' }} buttonTitle="submit" />);

    modalPendaftaran.handleChange({ target: { value: 'duar' } });
    expect(modalPendaftaran.state.coverLetter).to.equal('duar');
  });

  it('close without problem', () => {
    const modalPendaftaran = ReactTestUtils.renderIntoDocument(
      <ApplyModal id={4} data={{ key: 'value' }} buttonTitle="submit" />);

    modalPendaftaran.handleClose();
    expect(modalPendaftaran.state.modalOpen).to.equal(false);
  });

});
