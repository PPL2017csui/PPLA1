import React from 'react';
import ReactTestUtils from 'react-addons-test-utils';
import fetchMock from 'fetch-mock';
import ApplicantList from '../../components/ApplicantList';
import Applicant from '../../components/Applicant';

describe('ApplicantList', () => {

  const vacancy = {
    close_time: '2019-03-28T05:55:42Z',
    company: {
      address: 'kebayoran baru',
      id: 1,
      logo: null,
      name: 'tutup lapak',
    },
    created: '2017-03-28T07:05:47.128672Z',
    description: 'Lorem ipsum dolbh.',
    id: 1,
    name: 'Software Engineer',
    open_time: '2017-03-28T05:55:38Z',
    updated: '2017-03-28T07:34:13.122093Z',
    verified: true,
  }

  const student = {
    role: 'student',
    data: {
      url: 'http://localhost:8000/api/users/9/',
      username: 'muhammad.reza42',
      email: 'muhammad.reza42@ui.ac.id',
      is_staff: false,
      company: null,
      supervisor: null,
      student: {
        id: 3,
        user: {
          url: 'http://localhost:8000/api/users/9/',
          username: 'muhammad.reza42',
          email: 'muhammad.reza42@ui.ac.id',
          is_staff: false,
        },
        name: 'Muhammad R.',
        created: '2017-03-28T13:33:46.147241Z',
        updated: '2017-03-28T13:33:46.148248Z',
        npm: 1406543593,
        resume: null,
        phone_number: null,
        bookmarked_vacancies: [
          3,
        ],
        applied_vacancies: [
          3,
          1,
        ],
      },
    },
  };

  const response = [
    { id: 1, status: Applicant.APPLICATION_STATUS.ACCEPTED, student, vacancy },
    { id: 2, status: Applicant.APPLICATION_STATUS.BOOKMARKED, student, vacancy },
    { id: 3, status: Applicant.APPLICATION_STATUS.NEW, student, vacancy },
    { id: 4, status: Applicant.APPLICATION_STATUS.REJECTED, student, vacancy },
  ];

  fetchMock.restore();
  fetchMock.get('*', response);

  it('renders without problem', () => {
    const applicantList = ReactTestUtils.renderIntoDocument(
      <ApplicantList status={Applicant.APPLICATION_STATUS.ACCEPTED} />);
    expect(applicantList).to.exist;
  });

  it('can update status', () => {
    const applicantList = ReactTestUtils.renderIntoDocument(
      <ApplicantList status={Applicant.APPLICATION_STATUS.ACCEPTED} />);
    applicantList.setState({applications: response });

    expect(applicantList.state).to.not.equal(response);
    applicantList.updateStatus(1, Applicant.APPLICATION_STATUS.BOOKMARKED);
    expect(applicantList.state).to.not.equal(response);
  });
});

