import React from 'react';
import ReactTestUtils from 'react-addons-test-utils';
import CompanyVacancy from '../../components/CompanyVacancy';

describe('CompanyVacancy', () => {
  const response = {
    close_time: '2019-03-28T05:55:42Z',
    company: {
      address: 'kebayoran baru',
      id: 1,
      logo: null,
      name: 'tutup lapak',
    },
    created: '2017-03-28T07:05:47.128672Z',
    description: 'Lorem ipsum dolbh.',
    id: 3,
    name: 'Software Engineer',
    open_time: '2017-03-28T05:55:38Z',
    updated: '2017-03-28T07:34:13.122093Z',
    verified: true,
  };

  const response2 = {
    close_time: '2019-03-28T05:55:42Z',
    company: {
      address: 'kebayoran baru',
      id: 1,
      logo: 'pictures',
      name: 'tutup lapak',
    },
    created: '2017-03-28T07:05:47.128672Z',
    description: 'Lorem ipsum dolbh.',
    id: 3,
    name: 'Software Engineer',
    open_time: '2017-03-28T05:55:38Z',
    updated: '2017-03-28T07:34:13.122093Z',
    verified: true,
  };

  const response3 = {
    close_time: '2019-03-28T05:55:42Z',
    company: {
      address: 'kebayoran baru',
      id: 1,
      logo: 'pictures',
      name: 'tutup lapak',
    },
    created: '2017-03-28T07:05:47.128672Z',
    description: 'Lorem ipsum dolbh.',
    id: 3,
    name: 'Software Engineer',
    open_time: '2017-03-28T05:55:38Z',
    updated: '2017-03-28T07:34:13.122093Z',
    verified: false,
  };

  it('renders with logo without problem', () => {
     const companyVacancy = ReactTestUtils.renderIntoDocument(
       <CompanyVacancy data={response2} />);
     expect(companyVacancy).to.exist;
  });

  it('renders without logo without problem', () => {
    const companyVacancy = ReactTestUtils.renderIntoDocument(
      <CompanyVacancy data={response} />);
    expect(companyVacancy).to.exist;
  });

  it('renders without logo for unverified company without problem', () => {
    const companyVacancy = ReactTestUtils.renderIntoDocument(
      <CompanyVacancy data={response3} />);
    expect(companyVacancy).to.exist;
  });

  it('loads when delete button clicked', () => {
    const companyVacancy = ReactTestUtils.renderIntoDocument(
      <CompanyVacancy data={response} />);
    const button = ReactTestUtils.findRenderedDOMComponentWithTag(companyVacancy, 'Button');
    ReactTestUtils.Simulate.click(button);
    expect(companyVacancy.state.deleteLoading).to.equal(true);
  });
});
