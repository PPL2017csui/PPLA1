import React from 'react';
import { Segment, Header, Image, Container, Form, Button, Icon, TextArea } from 'semantic-ui-react';
import TopMenu from './components/TopMenu';

export default class CompanyProfile extends React.Component {

  static propTypes = {
    user: React.PropTypes.object.isRequired,
  };

  render() {
    const defaultLogo = 'https://semantic-ui.com/images/wireframe/square-image.png';
    const data = TopMenu.getInfo(this.props.user);
    return (
      <div className="companyProfile">
        <Segment className="segmentProfileCompany" >
          <Header as="h2" icon textAlign="center">
            <Image src={data.logo || defaultLogo} size="small" shape="circular" />
          </Header>
          <Container textAlign="center" className="profile-biodata">
            <div className="biodataCompany">
              <h2>{ data.name }</h2>
              <h3>{ data.address }t</h3>
              <p>{ data.description }</p>
            </div>
          </Container>
        </Segment>
        {/* <Segment className="profileFormCompany">*/}
        {/* <Header as="h3" textAlign="center">*/}
        {/* <Icon name="edit" />*/}
        {/* <Header.Content>*/}
        {/* Edit Profile Page*/}
        {/* </Header.Content>*/}
        {/* </Header>*/}
        {/* <Form size="small" >*/}
        {/* <Form.Field>*/}
        {/* <label htmlFor="photo">Logo Perusahaan</label>*/}
        {/* <input placeholder="Profile Photo.jpg" name="photo" type="File" />*/}
        {/* </Form.Field>*/}
        {/* <Form.Field>*/}
        {/* <label htmlFor="email">Nama Perusahaan</label>*/}
        {/* <input placeholder="Nama Perusahaan" name="email" />*/}
        {/* </Form.Field>*/}
        {/* <Form.Field>*/}
        {/* <label htmlFor="phone">Deskripsi</label>*/}
        {/* <TextArea placeholder='Try adding multiple lines' autoHeight />*/}
        {/* </Form.Field>*/}
        {/* <Button type="submit" size="small" primary floated="right">Submit</Button>*/}
        {/* </Form>*/}
        {/* </Segment>*/}
      </div>
    );
  }
}
